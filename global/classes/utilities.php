<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Utilities
{
    function __construct()
	{
		if (php_sapi_name() == "cli") {
			$this->base = dirname(dirname(dirname(__FILE__)));
		} else {
			$this->base = dirname($_SERVER['DOCUMENT_ROOT']);
		}
		require_once($this->base.'/global/functions.php');
		$this->logger = new Logging();
	}

    function convertFromUTC($date = NULL)
	{
		date_default_timezone_set('UTC');

		if(strlen($date) == 0)
		{
			$date = new DateTime();
		}
		else
		{
			$date = new DateTime($date);
		}

		if(!array_key_exists('time_zone', $_SESSION['user_info']['preferences']))
			$_SESSION['user_info']['preferences']['timezone']['value'] = date_default_timezone_get();

		$date->setTimezone(new DateTimeZone($_SESSION['user_info']['preferences']['timezone']['value']));
		return $date;
	}

	function convertPHPSizeToBytes($sSize)
	{
		if ( is_numeric( $sSize) ) {
		   return $sSize;
		}
		$sSuffix = substr($sSize, -1);
		$iValue = substr($sSize, 0, -1);
	       switch(strtoupper($sSuffix)){
    		case 'P':
    			$iValue *= 1024;
    		case 'T':
    			$iValue *= 1024;
    		case 'G':
    			$iValue *= 1024;
    		case 'M':
    			$iValue *= 1024;
    		case 'K':
    			$iValue *= 1024;
    			break;
    	}
		return $iValue;
	}

    public function createFolder($ia)
    {
        if(file_exists($ia['targetDir']) === false)
        {
            mkdir($ia['targetDir'], 0777, true);
        }
        // At least make sure it has the correct permissions
        $this->setPermissions($ia);
        return $ia['targetDir'].DIRECTORY_SEPARATOR;
    }

	function formatBytes($bytes, $precision = 2)
	{
		$units = array('B', 'KB', 'MB', 'GB', 'TB');

		$bytes = max($bytes, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);

		$bytes /= pow(1024, $pow);

		return round($bytes, $precision) . ' ' . $units[$pow];
	}

    function getSystemFreeSpace($file)
    {
        $si_prefix = ['B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB'];
        $base = 1024;

        $bytes_total_space = disk_total_space(dirname($file));
        $class = min((int)log($bytes_total_space , $base) , count($si_prefix) - 1);
        $total_space = sprintf('%1.2f' , $bytes_total_space / pow($base,$class));

        $bytes_used_space = disk_free_space(dirname($file));
        $base = 1024;
        $class = min((int)log($bytes_used_space , $base) , count($si_prefix) - 1);
        $total_used_space = sprintf('%1.2f' , $bytes_used_space / pow($base,$class));

        $inner_width = round((($total_used_space / $total_space) * 100), 2);

        return ['percent_free'=>$inner_width, 'used_space'=>$total_used_space, 'total_space'=>$total_space, 'size_unit'=>$si_prefix[$class]];
    }

    function getUTCDateTime()
	{
		date_default_timezone_set('UTC');
		$date = new DateTime();
		return $date;
	}

	function mailMessage($params=NULL)
	{
		if(!$params['to'] || !$params['subject'] || !$params['body']) die("A required field is missing. Please go back and try again.");

		if(!array_key_exists('from_address', $params) || (array_key_exists('from_address', $params) && $params['from_address'] == ''))
		{
			$params['from_address'] = (env == 'cli') ? $params['session_array']['site_config']['general_settings']['administrator_email']['value'] : $_SESSION['site_config']['general_settings']['administrator_email']['value'];
		}

		if(!array_key_exists('from', $params) || (array_key_exists('from', $params) && $params['from'] == '')) { $params['from'] = 'administrator'; }

		if(!array_key_exists('priority', $params) || (array_key_exists('priority', $params) && intval($params['priority']) == 0)) { $params['priority'] = '3'; }

		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->Host = (env == 'cli') ? $params['session_array']['site_config']['mail_server_settings']['smtp_server_address']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_server_address']['value'];	// SMTP server
		$mail->SMTPAuth = true; // turn on SMTP authentication
		$mail->SMTPSecure = "tls";
		$mail->Port = (env == 'cli') ? $params['session_array']['site_config']['mail_server_settings']['smtp_port']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_port']['value'];
		$mail->Username = (env == 'cli') ? $params['session_array']['site_config']['mail_server_settings']['smtp_user_name']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_user_name']['value'];	// SMTP username
		$mail->Password = (env == 'cli') ? $params['session_array']['site_config']['mail_server_settings']['smtp_password']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_password']['value']; // SMTP password
		$mail->SetFrom($params['from_address'], $params['from']);

		$mail->Priority = intval($params['priority']);
        if(isset($params['cc'])) { $mail->AddCC($params['cc']); }
		if(isset($params['bcc'])) { $mail->AddBCC($params['bcc']); }

		$mail_to = explode(',', $params['to']);
		foreach($mail_to as $address) { $mail->AddAddress(trim($address)); }

		$mail->Subject = "[".(env == 'cli' ? $params['session_array']['site_config']['general_settings']['app_name']['value'] : $_SESSION['site_config']['general_settings']['app_name']['value'])."] - ".$params['subject'];
		$mail->Body = $params['body'];
		$mail->MsgHTML($params['body']);

		if(isset($params['alt_body'])) { $mail->AltBody = $params['alt_body']; }

		$mail->isHTML(true);
		$mail->SetLanguage("en");

		if(!$mail->Send())
		{
			switch(env)
			{
				case 'cli':
					$logger = new Logging();
					$logger->write_to_log('error.log', $mail->ErrorInfo);
					echo $mail->ErrorInfo;
				break;

				default:
					if($mail->ErrorInfo != "SMTP Error: Data not accepted.")
					{
						$_SESSION['error'] = "mailer_too_busy";
						//display_message();
						exit();
					}
				break;
			}
		}
		else
		{
			$track_email = (env == 'cli') ? $params['session_array']['site_config']['advanced_settings']['track_email']['value'] : $_SESSION['site_config']['advanced_settings']['track_email']['value'];
			if($track_email == 1)
			{
				if(!isset($db_mail_conn))
					$db_mail_conn = new Database();

				$sql = "
					INSERT INTO email
						(`from`, `to`, `subject`, `date_registered`)
					VALUES
						('".$params['from_address']."', '".$params['to']."', '".$mail->Subject."', NOW())";
				//die($sql);
				$db_mail_conn->query($sql);
			}
		}

	    $mail->ClearAddresses();
		return;
	}

    public function print_p($message)
	{
		if(is_array($message))
		{
			echo '<pre>';
			print_r($message);
			echo '</pre>';
		}
		else
		{
			echo $message;
		}
	}

    public function setPermissions($ia)
	{
        //$this->logger->write_to_log('general.log', $ia);
        $userInfo = posix_getpwuid(posix_getuid());
        $user = $userInfo['name'];

        $groupInfo = posix_getgrgid(posix_getgid());
        $group = $groupInfo['name'];

        $user = 'broadcastr';
        $group = 'apache';
//        $this->logger->write_to_log('general.log', 'Trying to change the ownership of '.$ia['targetDir'].' to user = '.$user.' and group = '.$group);
//        $chmod = chmod($ia['targetDir'], 0777);
//        $chown = chown($ia['targetDir'], $user);
//        $chgrp = chgrp($ia['targetDir'], $group);

		return true;
	}

    public function tailCustom($filepath, $lines = 1, $adaptive = true)
    {
        // Modified from https://gist.github.com/gnovaro/0b739fe03d07da03fb79f2bcf7dea72e
        // Open file
        $f = @fopen($filepath, "rb");
        if ($f === false) return false;
        // Sets buffer size
        if (!$adaptive)
        {
            $buffer = 4096;
        }
        else
        {
            $buffer = ($lines < 2 ? 64 : ($lines < 10 ? 512 : 4096));
        }
        // Jump to last character
        fseek($f, -1, SEEK_END);
        // Read it and adjust line number if necessary
        // (Otherwise the result would be wrong if file doesn't end with a blank line)
        if (fread($f, 1) != "\n") $lines -= 1;
        // Start reading
        $output = '';
        $chunk = '';
        // While we would like more
        while (ftell($f) > 0 && $lines >= 0) {
            // Figure out how far back we should jump
            $seek = min(ftell($f), $buffer);
            // Do the jump (backwards, relative to where we are)
            fseek($f, -$seek, SEEK_CUR);
            // Read a chunk and prepend it to our output
            $output = ($chunk = fread($f, $seek)) . $output;
            // Jump back to where we started reading
            fseek($f, -mb_strlen($chunk, '8bit'), SEEK_CUR);
            // Decrease our line counter
            $lines -= substr_count($chunk, "\n");
        }
        // While we have too many lines
        // (Because of buffer size we might have read too many)
        while ($lines++ < 0) {
            // Find first newline and remove all text before that
            $output = substr($output, strpos($output, "\n") + 1);
        }
        // Close file and return
        fclose($f);
        return trim($output);
    }
}
