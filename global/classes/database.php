<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Database
{  
    public $setup, $linkId, $result=[], $error=false, $errorNum, $num_rows=0, $insert_id=null, $logger;

    function __construct($setup)
    {
        $this->setup        = $setup;
        $this->linkId;
        $this->result       = false;
        $this->error        = false;
        $this->errorNum     = false;
        $this->num_rows     = false;
        $this->insert_id    = false; 
        
        $this->logger       =  new Logger($setup);
    }

    //-------------------------------------------
    //    Connects to the database
    //-------------------------------------------
    public function connect()
    {
        if(!$this->linkId)
        {
            try
            {
                $dbname = (property_exists($this, 'No_DB')) ? '' : "dbname=".$this->setup->Database;
                $db_string = "mysql:host=".$this->setup->Host.";$dbname";
                $dbh = new PDO($db_string,$this->setup->User,$this->setup->Password);
            }
            catch(PDOException $e) {
                $this->halt($e->getMessage());
            }
            $this->linkId = $dbh;
            //$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        }

        if( !$this->linkId )
        {
            $this->halt( "Link-ID == false, connect failed" );
        }
    }
    // end function connect

    //-------------------------------------------
    //    Queries the database
    //-------------------------------------------
    public function query($sql, $options=NULL)
	{
        $this->connect();
        $this->result = NULL;

        try {
            $query = $this->linkId->prepare($sql);
        }
        catch(PDOException $e)
        {
            echo "SQL Error:\n";
            $this->linkId->errorInfo();
            $this->logger->write_to_log('sql.error.log', $this->linkId->errorInfo());
            $this->halt($e->getMessage());
            die();
        }

        if(!$query)
        {
            echo "SQL Error:\n";
            $this->linkId->errorInfo();
            $this->logger->write_to_log('sql.error.log', $this->linkId->errorInfo());
            die();
        }

        if(isset($options) && count($options) > 0 && array_key_exists('parameters', $options) && is_array($options['parameters']))
        {
            $bound_parameters = $options['parameters'];
        }

        if(isset($options['debug']) && $options['debug'] === true)
        {
            // Log out the sql query with any bound parameters
            if(isset($bound_parameters))
            {
                foreach($bound_parameters as $key=>$value)
                {
                    if(is_string($value))
                    {
                        $sql = str_replace($key, "'".addslashes($value)."'", $sql);
                    }
                    else
                    {
                        $sql = str_replace($key, addslashes($value), $sql);
                    }
                    
                }
            }
            $this->logger->write_to_log('sql_debug.log', $sql);
        }

        try {
            if(isset($bound_parameters) && is_array($bound_parameters) && count($bound_parameters) > 0)
            {
                $query->execute($bound_parameters);
            }
            else
            {
                $query->execute();
            }
        }
        catch(Exception $e)
        {
            echo "SQL Error";
            var_dump($e->getMessage());
            $this->logger->write_to_log('sql.error.log', "SQL:\n$sql\n\nError:\n".$e->getMessage());
        }

        $this->result = $query->FetchAll(PDO::FETCH_ASSOC);
        $this->insert_id = $this->linkId->lastInsertId();
        $this->num_rows = (is_array($this->result)) ? count($this->result) : null;

    } // end function query

    //-------------------------------------------
    //    If error, halts the program
    //-------------------------------------------
    function halt( $msg )
    {
        printf("<strong>Database error:</strong> %s<br>", $msg );
        printf("<strong>MySQL Error</strong>: %s (%s)<br>", $this->errorNum, $this->error);
        $this->logger->write_to_log('sql.error.log', 'MySQL Error: '.$msg);
        $this->logger->write_to_log('sql.error.log', $this->errorNum);
        $this->logger->write_to_log('sql.error.log', $this->error);

        die();
    } // end function halt

    function debug()
    {
        //return $this->result->debugDeumpParams();
    }
}