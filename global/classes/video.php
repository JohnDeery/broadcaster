<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Video
{
    function __construct()
    {
        require_once(dirname(dirname(__FILE__)).'/functions.php');
        $configuration['site_config'] = config_site();

        $this->db = new Database();
        $this->logger = new Logging();
        $this->api = new API();
        $this->utilities = new Utilities();

        $this->ffmpeg_cmd = $configuration['site_config']['advanced_settings']['ffmpeg_location']['value'].DIRECTORY_SEPARATOR.'ffmpeg';
        $this->ffprobe_cmd = $configuration['site_config']['advanced_settings']['ffmpeg_location']['value'].DIRECTORY_SEPARATOR.'ffprobe';

        $this->originalVideosDir = $this->utilities->createFolder(['targetDir'=>$this->api->base.DIRECTORY_SEPARATOR.'original_videos']);
        $this->publicVideosDir = $this->utilities->createFolder(['targetDir'=>$this->api->base.DIRECTORY_SEPARATOR.'public_html'.DIRECTORY_SEPARATOR.'videos']);
        $this->endcodingLogsDir = $this->utilities->createFolder(['targetDir'=>$this->api->base.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'encoding_logs']);
    }

    public function createSymLink($info)
    {
        symlink(base.'/videos/'.$info['id'], public_html.'videos/'.$info['id']);
    }

    public function deleteVideo($request_info)
    {
        $request_info['allowed_verbs'] = array("DELETE");
        if($this->api->checkVerbs($request_info) === FALSE)
        {
            return array('error'=>array('http_code'=> '403', 'message'=>'Method not allowed. Valid methods are: '.implode(', ', $request_info['allowed_verbs'])));
        }

        // Give them 24 hours to change their mind
        $delete_timestamp = $this->utilities->getUTCDateTime()->modify('+1 Day');
        $delete_timestamp = $delete_timestamp->format('Y-m-d h:i:s');

        $sql = "
            UPDATE
                videos
            SET
                status = 'pending_delete'
                , delete_timestamp = :delete_timestamp
            WHERE
                uid = :uid
        ";
        $options['parameters'] = array(':uid'=>$request_info['endpoint_info']['uid'], ':delete_timestamp'=>$delete_timestamp);
        $result = $this->db->query($sql, $options);

        return array('msg'=>'video queued for delete');
    }

    public function getAllVideos($request_info)
    {
        $request_info['allowed_verbs'] = array("GET");
        if($this->api->checkVerbs($request_info) === FALSE)
        {
            return array('error'=>array('http_code'=> '403', 'message'=>'Method not allowed. Valid methods are: '.implode(', ', $request_info['allowed_verbs'])));
        }

        if(isset($_SESSION['user_info']['permissions']) )
        {
            $where = '1=1';
            $parameters = [];
        } else {
            if(isset($_SESSION['user_info']['id']) && intval($_SESSION['user_info']['id']) > 0)
            {
                $where = 'owner = :user_id';
                $parameters[':user_id'] = $_SESSION['user_info']['id'];
            }
        }

        $sql = "
            SELECT
                v.title
                , v.description
                , v.duration
                , v.uid
                , v.status
                , v.date_uploaded
            FROM
                videos v
            WHERE
                $where
        ";
        $options['parameters'] = $parameters;
        $this->db->query($sql, $options);

        if($this->db->num_rows() == 0)
        {
            return [];
        }

        foreach($this->db->result as $row)
        {
            $return[$row['uid']] = array(
                'title' => $row['title'],
                'description' => $row['description'],
                'duration' => $row['duration'],
                'status' => $row['status'],
                'date_uploaded' => $row['date_uploaded']
            );

            if(file_exists($this->api->base.DIRECTORY_SEPARATOR.'public_html'.DIRECTORY_SEPARATOR.'videos'.DIRECTORY_SEPARATOR.$row['uid'].DIRECTORY_SEPARATOR.$row['uid'].'.zip'))
            {
                $return[$row['uid']]['download'] = DIRECTORY_SEPARATOR.'videos'.DIRECTORY_SEPARATOR.$row['uid'].DIRECTORY_SEPARATOR.$row['uid'].'.zip';
            }
            $return[$row['uid']]['poster'] = (file_exists($this->api->base.'/public_html/videos/'.$row['uid'].'/thumbnail.jpg')) ? '/videos/'.$row['uid'].'/thumbnail.jpg' : '/images/thumbnail_not_found.png';
        }

        return $return;
    }

    public function getConvertCmd($request_info)
    {
/*
        $sql = "
    		SELECT
    			cmd
    			, extension
    			, media_type
    		FROM
    			encoding_profiles
    		WHERE
    			profile_name IN ('".implode("','", $profiles)."')
    	";
        $db->query
        */
        return $this->ffmpeg_cmd.' -hide_banner -y -progress "'.$this->endcodingLogsDir.DIRECTORY_SEPARATOR.$request_info['uid'].'.'.$request_info['extension'].'.progress.log" -i "'.$request_info['filePath'].'" '.$sub_cmd.' "'.$request_info['finalFilePath'].$request_info['uid'].'.'.$profile['extension'].'" > "'.$this->endcodingLogsDir.DIRECTORY_SEPARATOR.$request_info['uid'].'.'.$profile['extension'].'.log" 2>&1';
    }

 	public function getMaximumFileUploadSize()
	{
		return min($this->utilities->convertPHPSizeToBytes(ini_get('post_max_size')), $this->utilities->convertPHPSizeToBytes(ini_get('upload_max_filesize')));
	}

    public function getPendingImports($request_info)
    {
        $request_info['allowed_verbs'] = array("POST");
		if($this->api->checkVerbs($request_info) === FALSE)
		{
			return array('error'=>array('http_code'=> '403', 'message'=>'Method not allowed. Valid methods are: '.implode(', ', $request_info['allowed_verbs'])));
		}

        $authorized = $this->api->checkAuthorization($request_info['headers'], $request_info['endpoint_info']);
        if(is_array($authorized) && array_key_exists('error', $authorized))
        {
			return array('error'=>array('http_code'=> $authorized['error']['http_code'], 'message'=>$authorized['error']['message']));
        }

        // Find all files in the upload directory assigned to this users
        $sql = "
            SELECT
                ftp_user
            FROM
                users
            WHERE
                id = :user_id
        ";
        $options['parameters'] = [':user_id' => $_SESSION['user_info']['id']];
        $this->db->query($sql, $options);

        if($this->db->num_rows() === 0 || strlen($this->db->result[0]['ftp_user']) === 0)
        {
            return ['msg'=>'No videos found for import'];
        }

        // We should now have an FTP user we can search on
        $cmd = "find ".$this->api->base.DIRECTORY_SEPARATOR."upload/ -user ".$this->db->result[0]['ftp_user'];
        exec($cmd, $files);
        if(!is_array($files) || count($files) === 0)
        {
            return [];
        }

        foreach($files as $file)
        {
            $fileInfo = pathinfo($file);
            $finalFiles[] = $fileInfo['basename'];
        }
        return $finalFiles;
    }

	public function getVideoDetails($request_info)
	{
		$db = new Database();
		$logger = new Logging();
		$api = new Api();

		$request_info['allowed_verbs'] = array("POST");
		if($api->checkVerbs($request_info) === FALSE)
		{
			return array('error'=>array('http_code'=> '403', 'message'=>'Method not allowed. Valid methods are: '.implode(', ', $request_info['allowed_verbs'])));
		}

		$authorized = $api->checkAuthorization($request_info['headers'], $request_info['endpoint_info']);

		if(is_array($authorized) && array_key_exists('error', $authorized))
        {
			return array('error'=>array('http_code'=> $authorized['error']['http_code'], 'message'=>$authorized['error']['message']));
        }

		$sql = "
			SELECT
				v.status
				, v.title
				, v.description
				, v.uid
				, v.attributes
                , v.duration
				, v.frames
			FROM
				videos v
			WHERE
				v.uid = :uid
		";
		$options['parameters'] = array(':uid'=>$request_info['post']['id']);
		$db->query($sql, $options);

		if($db->num_rows() == 0)
		{

		}

		$row = $db->result[0];

		$video_details['title'] = $row['title'];
		$video_details['description'] = $row['description'];
		$video_details['duration'] = $row['duration'];
        $video_details['frames'] = $row['frames'];

		// Player attributes
		$attributes = json_decode($row['attributes'], TRUE);

		if(!is_array($attributes) || count($attributes) === 0) $attributes = array();

		// set all the defaults
		$video_details['player'] = array('fluid'=>'true', 'preload'=>'auto', 'loop'=>FALSE, 'controls'=>TRUE, 'autoplay'=>FALSE);

		if(array_key_exists('player', $attributes))
		{
			// Ok, check each of the main settings and see if we have anything, otherwise, the defaults. There is probably a cleaner way to do this
			if(array_key_exists('width', $attributes['player']))
				$video_details['player']['width'] = $attributes['player'];

			if(array_key_exists('height', $attributes['player']))
				$video_details['player']['height'] = $attributes['player']['height'];

			if(array_key_exists('preload', $attributes['player']))
				$video_details['player']['preload'] = $attributes['player']['preload'];

			if(array_key_exists('loop', $attributes['player']))
				$video_details['player']['loop'] = $attributes['player']['loop'];

			if(array_key_exists('controls', $attributes['player']))
				$video_details['player']['controls'] = $attributes['player']['controls'];

			if(array_key_exists('autoplay', $attributes['player']))
				$video_details['player']['autoplay'] = $attributes['player']['autoplay'];
		}

		$video_details['status'] = $row['status'];

		if($row['status'] == 'ready')
		{
			// Build out the actual movie files for the HTML5 tags
			$sql = "
				SELECT
					eq.extension
					, eq.media_type
				FROM
					encoding_queue eq
				WHERE
					eq.uid = :uid
					AND eq.status = :status
                ORDER BY
                    extension DESC
			";
			$options['parameters'] = array(':uid'=>$row['uid'], ':status'=>'finished');
			$db->query($sql, $options);

			if($db->num_rows() == 0)
			{
				$logger->write_to_log('error.log', 'Something went wrong, there were no finished videos but the status is ready!');
				return;
			}

			foreach($db->result as $video_row)
			{
				if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/videos/'.$row['uid'].'/'.$row['uid'].'.'.$video_row['extension']))
				{
					$logger->write_to_log('error.log', 'We should have a video with extension '.$video_row['extension'].' but it was not found');
				}
				else
				{
					$video_details['video_files'][$video_row['media_type']] = '/videos/'.$row['uid'].'/'.$row['uid'].'.'.$video_row['extension'];
				}
			}

			// check and see if we have a poster image to show
			if(file_exists($_SERVER['DOCUMENT_ROOT'].'/videos/'.$row['uid'].'/thumbnail.jpg'))
				$video_details['poster'] = '/videos/'.$row['uid'].'/thumbnail.jpg';

            // See if we have a zip file for easy download
            if(file_exists($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'videos'.DIRECTORY_SEPARATOR.$row['uid'].DIRECTORY_SEPARATOR.$row['uid'].'.zip'))
				$video_details['zipfile'] = DIRECTORY_SEPARATOR.'videos'.DIRECTORY_SEPARATOR.$row['uid'].DIRECTORY_SEPARATOR.$row['uid'].'.zip';
		}
		return $video_details;
	}

	public function buildVideoPlayer($id, $config)
	{
		$db = new Database();
		$logger = new Logging();

		$video_details = $this->getVideoDetails($id, $config);

		switch($video_details['status'])
		{
			case 'ready':
				$videos = array();

				// Set up the data-setup array with anything we've defined in the attributes of this video, minus the above poster
				$data_setup = json_encode($video_details['player']);

				$return = '
					<link href="/includes/javascript/video-js/video-js.css" rel="stylesheet" type="text/css">
					<div id="video_area" class="text-center">
						<video id="'.$row['uid'].'" class="video-js vjs-default-skin vjs-big-play-centered center" data-setup=\''.$data_setup.'\'>
				';

					if(!is_array($videos) || count($videos) == 0)
						return;

					foreach($videos as $key=>$value)
					{
						$return.= '
								<source src="'.$value.'" type="'.$key.'" />
						';
					}
					$return.='
							</video>
						</div>

						<script src="/includes/javascript/video-js/video.js"></script>
						<script src="/includes/javascript/video-js/plugins/videojs.disableProgress.js"></script>
						<script src="/includes/javascript/video_script.js"></script>
				';
				return $return;
			break;

			default:
				$return = '
					<div id="video_area" class="text-center">
						<div id="video_not_found" class="center">Sorry, but there doesn\'t seem to be a video with that ID</div>
					</div>
				';
	?>
				<h1><?php echo stripslashes($row['title']); ?></h1>
				<div id="video_not_found">This video is currently encoding and will be available soon!</div>
	<?php
			break;
		}
	}

	public function generateThumbnail($video_array)
	{
		$cmd = $this->ffmpeg_cmd." -ss 00:00:03 -y -i '".$video_array['top_dir']."/original_videos/".$video_array['file']."' -vframes 1 -q:v 2 '".$video_array['full_video_path']."thumbnail.jpg'";
        $this->logger->write_to_log("encoding.log", $cmd);
		exec($cmd, $output, $encoding_result);
		$this->utilities->setPermissions(['target'=>$video_array['full_video_path']."thumbnail.jpg"]);
		return TRUE;
	}

	public function generateUniqueId()
	{
		$uid = NULL;

		$characters = "abcdefghijklmnopqrstuvwxyz0123456789-_";

		for ($i=0;$i < rand(10, 20);$i++)
		{
			$tmp = substr($characters, rand() % 33, 1);
			if((rand() % 33) < 10 ) { $tmp = strtoupper($tmp); }
			$uid = $uid . $tmp;
		}

        while(file_exists($this->api->base.DIRECTORY_SEPARATOR.'public_html'.DIRECTORY_SEPARATOR.'videos'.DIRECTORY_SEPARATOR.$uid))
        {
            $uid = generate_unique_id();
        }

		return $uid;
	}

	public function getCurrentlyEncoding()
	{
		$sql = "
			SELECT
				eq.id
			FROM
				encoding_queue eq
			WHERE
                eq.encoding_server = :server_name
                AND eq.status = :status
		";
		$options['parameters'] = array(':status'=>'encoding', ':server_name'=>$this->api->server_name);
		$this->db->query($sql, $options);
		return $this->db->num_rows();
	}

	public function getEncodingProgress($request_array)
	{
        $this->logger->write_to_log("general.log", 'Request Array: '.print_r($request_array, true));
		$sql = "
			SELECT
				CONCAT(uid, '.', extension) AS 'log_name'
				, extension
				, status
			FROM
				encoding_queue
			WHERE
				uid = :uid
				AND status != :status
			LIMIT 1
		";
		$options['parameters'] = array(':uid' => $request_array['post']['id'], ':status'=>'finished');
		$this->db->query($sql, $options);

		if($this->db->num_rows() == 0)
        {
            return ['status'=>'done'];
        }

		$result = $this->db->result[0];

		if($result['status'] == 'pending')
		{
            return ['status'=>'pending', 'file_type'=>$result['extension']];
        }

        $return = [
            'file_type' =>  $result['extension'],
            'progress' => 0,
            'status' => 'continue'
        ];

        $file = $this->api->base.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'encoding_logs'.DIRECTORY_SEPARATOR.$result['log_name'].'.progress.log';

        $progress_cmd = "tac $file | grep -m 1 frame= | awk '{print $1}' | cut -c 7-";
        exec($progress_cmd, $progress);
        //$this->logger->write_to_log('general.log', $progress_cmd);
        //$this->logger->write_to_log('general.log', $progress);
        if((isset($progress[0]) && $progress[0] > 0) && (isset($request_array['endpoint_info']['frames']) && $request_array['endpoint_info']['frames'] > 0))
        {
            $return['progress'] = round(((intval($progress[0]) / $request_array['endpoint_info']['frames']) * 100), 0);
        }

        $status_cmd = "tac $file | grep -m 1 progress= | awk '{print $1}' | cut -c 10-";
        exec($status_cmd, $status);
        //$this->logger->write_to_log('general.log', $status_cmd);
        //$this->logger->write_to_log('general.log', $status);

        if(isset($status[0]) && strlen($status[0]) > 0)
        {
            $return['status'] = $status[0];
        }

		return $return;
	}

	public function getOriginalVideoDetails($request_array = NULL)
	{
		$video_details_cmd = $this->ffprobe_cmd.' -v error -select_streams v:0 -sexagesimal -show_format -show_streams -print_format json '.$request_array['filePath'].' 2>&1';
		$video_details = shell_exec($video_details_cmd);
		$video_details = json_decode($video_details, TRUE);

		if(isset($video_details['streams'][0]['nb_frames']))
		{
			$total_frames = $video_details['streams'][0]['nb_frames'];
		}
		else
		{
			$total_frames_command = $this->ffprobe_cmd.' -v error -show_packets '.$request_array['filePath'].' 2>&1 | grep video | wc -l';
			$total_frames = shell_exec($total_frames_command);
		}

		$video_details['total_frames'] = $total_frames;

		return $video_details;
	}

	public function getNextVideoForConversion()
	{
		$sql = "
			SELECT
				v.id
				, v.uid
				, v.original_name
				, v.original_ext
				, u.email_address
				, eq.cmd
				, eq.id AS 'queue_id'
				, eq.extension
			FROM
				videos v
				INNER JOIN encoding_queue eq ON eq.uid = v.uid
				INNER JOIN users u ON u.id = v.owner
			WHERE
				v.status IN('pending', 'processing')
				AND eq.status = 'pending'
                AND (
                    eq.encoding_server IS NULL
                    OR eq.encoding_server = :server_name
                )
			ORDER BY
				queue_priority DESC
                , id ASC
			LIMIT 1
		";
        //$options['debug'] = true;
        $options['parameters'] = [':server_name' => $this->api->server_name];
		$this->db->query($sql, $options);

		if($this->db->num_rows() > 0)
		{
			return $this->db->result[0];
		}
		return false;
	}

    public function processVideos($request_info = [])
    {
        // take an array of files passed in and process them

        // Do some inits first, make sure we have the appropriate directories created
        $originalVideosDir = $this->utilities->createFolder(['targetDir'=>$this->api->base.DIRECTORY_SEPARATOR.'original_videos']);
        $publicVideosDir = $this->utilities->createFolder(['targetDir'=>$this->api->base.DIRECTORY_SEPARATOR.'public_html'.DIRECTORY_SEPARATOR.'videos']);
        $endcodingLogsDir = $this->utilities->createFolder(['targetDir'=>$this->api->base.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'encoding_logs']);

        $this->logger->write_to_log('general.log', $request_info['post']['files']);

        if(!isset($request_info['post']['files']) || !is_array($request_info['post']['files']) || count($request_info['post']['files']) === 0)
        {
            return ['error'=>['http_code'=> '403', 'message'=>'No files found in request.']];
        }

        foreach($request_info['post']['files'] as $file)
        {
            unset($file_info);

            $uploadFilePath = $this->api->base.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.$file;
        	$uid = $this->generateUniqueId();
        	$owner = 1;

            // Get information about the uploaded file
        	$file_info = pathinfo($uploadFilePath);

            if(isset($request_info['post']['source']) && $request_info['post']['source'] === 'bulk_importer')
            {
                // Try to get the broadcastr user id from the file owner, which only really applies in the bulk upload scenario. Do this before we move the file and change it's owner
            	$file_owner = posix_getpwuid(fileowner($uploadFilePath));
            	$file_owner = (isset($file_owner['name']) && strlen($file_owner['name']) > 0) ? $file_owner['name'] : '';

            	if(isset($file_owner) && strlen($file_owner) > 0)
            	{
            		$sql = "
            			SELECT
            				id
            			FROM
            				users
            			WHERE
            				ftp_user = :file_owner
            		";
            		$options['parameters'] = array(':file_owner'=>$file_owner);
            		$this->db->query($sql, $options);

            		if($this->db->num_rows() === 1)
            		{
            			$owner = $this->db->result[0]['id'];
            		}
            	}
            }

            // Switch this to a sql lookup since files might exist on different servers
            /*
        	while(file_exists($api->base.DIRECTORY_SEPARATOR.'videos'.DIRECTORY_SEPARATOR.$uid.'.'.$file_info['extension']))
            {
        		$uid = generate_unique_id();
            }
            */

            // Put the file in a path that is not in the web accessible root.
            $filePath = $originalVideosDir.$uid.'.'.$file_info['extension'];
//        	$move = rename("$uploadFilePath", "$filePath"); // I think this is causing issues re: permissions, let's see if this fixes it
            $copy = copy("$uploadFilePath", "$filePath");

            if($copy === false)
            {
                print_r("FAILED TO MOVE FILE FROM UPLOAD FOLDER TO ORIGINAL_VIDEOS FOLDER, FAILING FOR THIS FILE\n");
                continue;
            }

            unlink("$uploadFilePath");

        	// Make the final path directory
            $finalFilePath = $this->utilities->createFolder(['targetDir'=>$publicVideosDir.$uid]);

        	$video_details = $this->getOriginalVideoDetails(['filePath'=>$filePath]);

        	$insert_video_array = array(
        		'uid'				=>	$uid
        		, 'original_name'	=>	$file
        		, 'file_extension'	=>	$file_info['extension']
        		, 'file_size'		=>	$video_details['format']['size']
                , 'duration'        =>  $video_details['format']['duration']
        		, 'total_frames'	=>	$video_details['total_frames']
        		, 'filename'		=>	$file_info['filename']
        		, 'owner'			=>	$owner
        	);

        	$initialInsert = $this->setInitialVideoDetails($insert_video_array);

        	// Get the proper encoding types and their commands, then copy into the encoding_queue table
        	$sql = "
        		SELECT
        			profiles_to_run
        		FROM
        			encoding_config
        		WHERE
        			file_type = :file_type
        	";
        	$options['parameters'] = array(':file_type'=>$file_info['extension']);
        	$this->db->query($sql, $options);

        	if($this->db->num_rows() == 1)
        	{
        		$profiles = json_decode($this->db->result[0]['profiles_to_run'], TRUE);
        	}
        	else
        	{
        		// get the defaults
        		$sql = "
        			SELECT
        				profiles_to_run
        			FROM
        				encoding_config
        			WHERE
        				file_type = '*'
        		";
        		$this->db->query($sql, $options);
        		if($this->db->num_rows() == 0)
        		{
        			echo 'Error: No profiles to run for this file. Define some then try again';
        			return;
        		}
        		$profiles = json_decode($this->db->result[0]['profiles_to_run'], TRUE);
        	}

        	// Get the profiles and break out the commands
        	$sql = "
        		SELECT
        			cmd
        			, extension
        			, media_type
        		FROM
        			encoding_profiles
        		WHERE
        			profile_name IN ('".implode("','", $profiles)."')
        	";
        	$this->db->query($sql);
        	if($this->db->num_rows() == 0)
        	{
        		echo 'Error: Could not parse the encoding profiles, aborting.';
        		return;
        	}

        	foreach($this->db->result as $profile)
        	{
        		$sql = "
        			INSERT INTO
        				encoding_queue
        			SET
        				uid = :uid
        				, cmd = :cmd
        				, extension = :extension
        				, media_type = :media_type
        				, status = :status
        				, date_created = :date_created
        		";
        		$cmd = json_decode($profile['cmd'], TRUE);

        		foreach($cmd as $sub_cmd)
        		{
        			// Add each sub command into the larger command to place into the queue table.
                    $regular_log = '"'.$endcodingLogsDir.$uid.'.'.$profile['extension'].'.log"';
                    $progress_log = '"'.$endcodingLogsDir.$uid.'.'.$profile['extension'].'.progress.log"';
                    $finalFileWithExtension = '"'.$finalFilePath.$uid.'.'.$profile['extension'].'"';
                    $master_command[] = "$this->ffmpeg_cmd  -hide_banner -y -progress $progress_log -i \"$filePath\" $sub_cmd $finalFileWithExtension > $regular_log 2>&1";
        		}
        		$now = $this->utilities->getUTCDateTime();
        		$options = ['parameters'=>[':uid'=>$uid, ':cmd'=>implode(" && ", $master_command), ':extension'=>$profile['extension'], ':media_type'=>$profile['media_type'], ':status'=>'pending', ':date_created'=>$now->format("Y-m-d h:i:s")]];
        		$this->db->query($sql, $options);
        		unset($master_command);
        	}
        	$this->logger->write_to_log('bulk_import.log', 'Imported '.$file.' as '.$uid);
        }
        return ['msg'=>'Videos successfully imported'];
    }

    public function saveBasicInfo($request_array)
    {
        $sql = "
            UPDATE
                videos
            SET
                title = :title
                , description = :description
            WHERE
                uid = :uid
        ";
        $options['parameters'][':title'] = (strlen($request_array['endpoint_info']['title']) > 0) ? $request_array['endpoint_info']['title'] : NULL;
        $options['parameters'][':description'] = (strlen($request_array['endpoint_info']['description']) > 0) ? $request_array['endpoint_info']['description'] : NULL;
        $options['parameters'][':uid'] = (strlen($request_array['endpoint_info']['id']) > 0) ? $request_array['endpoint_info']['id'] : NULL;

        $this->db->query($sql, $options);
        return true;
    }

	public function setInitialVideoDetails($request_array = NULL)
	{
		$sql = "
            INSERT INTO
                videos
            SET
                uid = :uid
                , original_name = :original_name
                , original_ext = :original_extension
                , file_size = :file_size
                , duration = :duration
                , frames = :frames
                , title = :title
                , owner = :owner
                , status = 'pending'
                , date_uploaded = :date_created
		";
        $now = $this->utilities->getUTCDateTime()->format('Y-m-d H:i:s');

        // trim miliseconds off the duration
        $videoDuration = explode('.', $request_array['duration']);
        $request_array['duration'] = $videoDuration[0];

        $options['parameters']= [
            ':uid'                  =>  $request_array['uid'],
            ':original_name'        =>  (isset($request_array['original_name']) ? $request_array['original_name'] : NULL),
            ':original_extension'   =>  (isset($request_array['file_extension']) ? $request_array['file_extension'] : NULL),
            ':file_size'            =>  intval($request_array['file_size']),
            ':duration'             =>  (isset($request_array['duration']) ? $request_array['duration'] : NULL),
            ':frames'               =>  intval($request_array['total_frames']),
            ':title'                =>  (isset($request_array['filename']) ? $request_array['filename'] : NULL),
            ':owner'                =>  (isset($request_array['owner']) ? $request_array['owner'] : 0),
            ':date_created'         =>  $now
        ];
        $this->db->query($sql, $options);
        return $this->db;
	}

    public function undeleteVideo($request_info)
    {
        $request_info['allowed_verbs'] = array("DELETE");
        if($this->api->checkVerbs($request_info) === FALSE)
        {
            return array('error'=>array('http_code'=> '403', 'message'=>'Method not allowed. Valid methods are: '.implode(', ', $request_info['allowed_verbs'])));
        }

        $sql = "
            UPDATE
                videos
            SET
                status = 'ready'
                , delete_timestamp = NULL
            WHERE
                uid = :uid
        ";
        $options['parameters'] = array(':uid'=>$request_info['endpoint_info']['uid']);
        $result = $this->db->query($sql, $options);

        return array('msg'=>'video undeleted');
    }

    public function updateVideoStatus($request_info)
    {
        $sql = "
            UPDATE
                videos
            SET
                `status` = :status
            WHERE
                uid = :uid
        ";
        $options['parameters'] = [':status' => $request_info['status'], ':uid'=>$request_info['uid']];
        $this->db->query($sql, $options);
    }

    public function updateEncodingStatus($request_info=[])
    {
        foreach($request_info as $key=>$value)
        {
            if($key !== 'id')
            {
                $sets[] = "`$key` = :$key";
            }
            $params[":$key"] = $value;
        }

        $sql = "
            UPDATE
                encoding_queue
            SET
                ".implode(", ", $sets)."
            WHERE
                id = :id
        ";
        $options['parameters'] = $params;
        $this->db->query($sql, $options);
    }

    public function uploadCaption($request_info)
    {
        $request_info['allowed_verbs'] = array("PUT");
        if($this->api->checkVerbs($request_info) === FALSE)
        {
            return array('error'=>array('http_code'=> '403', 'message'=>'Method not allowed. Valid methods are: '.implode(', ', $request_info['allowed_verbs'])));
        }

        $this->logger->write_to_log('general.log', $request_info);
    }

    public function uploadVideo($request_info)
    {
        $request_info['allowed_verbs'] = array("PUT");
        if($this->api->checkVerbs($request_info) === FALSE)
        {
            return array('error'=>array('http_code'=> '403', 'message'=>'Method not allowed. Valid methods are: '.implode(', ', $request_info['allowed_verbs'])));
        }

        $this->logger->write_to_log('general.log', $requset_info);
die();
/*
        // Accept a base64 encoded string, which we'll turn back into a movie file
        $data = file_get_contents('php://input');
        $data = base64_decode($data);

        $targetDir = $this->utilities->createFolder(['targetDir'=>$this->api->base.'original_videos']);

        // Check and make sure we have a video directory
        $this->utilities->createFolder(['targetDir'=>$this->api->base.'public_html'.DIRECTORY_SEPARATOR.'videos']);

        // Put the file in a path that is not in the web accessible root.
        $filePath = $targetDir.DIRECTORY_SEPARATOR.$tmp_file_name;

        $fileInfo = pathinfo("$filePath");
        $uid = $video->generateUniqueId();
        $newFilePath = $targetDir.$uid.'.'.$fileInfo['extension'];

        rename("$filePath", "$newFilePath");

        $finalFilePath = $utilities->createFolder(['targetDir'=>$api->base.DIRECTORY_SEPARATOR.'public_html'.DIRECTORY_SEPARATOR.'videos'.DIRECTORY_SEPARATOR.$uid]);
*/

    }
}
