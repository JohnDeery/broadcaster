<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Logger
{
	private $setup;

	function __construct($setup)
	{
		if(empty($setup))
		{
			return false;
		}
		else
		{
			$this->setup = $setup;
		}

		$this->setup->base = dirname($_SERVER['DOCUMENT_ROOT']);

        if (php_sapi_name() === "cli") {
            $this->setup->base = dirname(__FILE__, 3);
        }
	}
	
	function cli_log(string $file_name = null, mixed $message = null) : bool
	{
		if(is_array($message))
		{
			$message = print_r($message, TRUE);
		}

		$file_name = $this->setup->base.'/logs/'.$file_name;
		file_put_contents($file_name, "[".date('Y-m-d h:i:s')."] - $message\n", FILE_APPEND);
		return true;
	}

	function write_to_log(string $log_file = NULL, mixed $message = NULL, array $email_options = NULL) : bool
	{
		if(empty($log_file))
		{
			error_log('No log file passed to logging class', 0);
			return false;
		}

        if(empty($message))
		{
			error_log('No message passed to logging class', 0);
			return false;
		}
		
		date_default_timezone_set('UTC');
		
		if(is_array($message))
		{
			$message = print_r($message, TRUE);
		}

		$log_file = $this->setup->base.'/logs/'.$log_file;			
		$message = "[".date('Y-m-d H:i:s')."] - $message\n";
		error_log($message, 3, $log_file);

/*		if(is_array($email_options) && count($email_options) > 0)
		{
			// also send as email to the place defined in the options

			$ia = [
				'to' => (array_key_exists('to', $email_options)) ? $email_options['to'] : '',
				'from' => 'admin',
				'from_address' => 'john.deery@gmail.com',
				'priority' => (array_key_exists('priority', $email_options)) ? $email_options['priority'] : 7,
				'subject' => (array_key_exists('subject', $email_options)) ? $email_options['subject'] : 'Log Message',
				'body' => $message
			];

			$this->mailMessage($ia);
		}
*/
		return true;
	}
}