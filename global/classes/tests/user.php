<?php
	require_once('../user.php');
	require_once('../api.php');

	$user = new User();

	$array = array(
        	'user_name'     =>  'whitney.snyder',
	        'first_name'    =>  'Whitney',
        	'last_name'     =>  'Snyder',
	        'email_address' =>  'wsnyder@tenstreet.com',
	        'mail_user'     =>  true
	);

	$user->createUser($array);
