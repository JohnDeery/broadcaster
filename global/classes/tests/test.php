<?php

$base = dirname(dirname(dirname(dirname(__FILE__))));

require_once($base.'/global/functions.php');
config_site(array("app_info", "site_config"));

$db = new Database;
$video = new Video;
$utilities = new Utilities;
$logger = new Logging;

$finalFilePath = "/srv/www/broadcastr.me/upload/test.txt";

$file_owner = posix_getpwuid(fileowner($finalFilePath));
$file_owner = (isset($file_owner['name']) && strlen($file_owner['name']) > 0) ? $file_owner['name'] : '';

if(isset($file_owner) && strlen($file_owner) > 0)
{
    $sql = "
        SELECT
            id
        FROM
            users
        WHERE
            ftp_user = :file_owner
    ";
    $options['parameters'] = array(':file_owner'=>$file_owner);
    $db->query($sql, $options);

    if($db->num_rows() === 1)
    {
        $owner = $db->result[0]['id'];
    }
}

print_r($owner);