<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class API
{
    public object $database, $logger;
    
    function __construct($database, $logger)
    {
        $this->database = $database;
        $this->logger = $logger;
    }

    function checkAuthorization(array $headers=[], array $endpoint_info=[])
    {
       if(!isset($headers['Authorization']))
       {
           return ['http_code'=>'403', 'message'=>'Authorization Header missing'];
       }
    
        $sql = "
            SELECT
                private_key
            FROM
                users
            WHERE
                public_key = :public_key
       ";
        $options['parameters'] = [':public_key'=>$headers['Authorization']];
        $this->database->query($sql, $options);
    
        if($this->database->num_rows != 1)
        {
            return ['http_code'=>'403', 'message'=>'Unauthorized. Please try again.'];
        }
    
        $privateKey = $this->database->result[0]['private_key'];
    
        $currentTime = strtotime('now');
        $requestTimeDiff = $currentTime - $headers['timestamp'];
    
        $this->logger->write_to_log('api.log', 'Current Time: '.$currentTime.', Sent Time: '.$headers['timestamp'].', Diff = '.$requestTimeDiff);
    
        if($requestTimeDiff < -20 || $requestTimeDiff > 20)
		{
            return ['http_code'=>'404', 'message'=>'Time drift too much, please send request again'];
		}
    
        // Rebuild the request with private key and see if it matches the Authorization Header
        /* Generate the Authorization Key:
        * 1. HTTP method verb (GET, POST, PUT)
        * 2. The Absolute Path part of the HTTP request URI excluding query string parameters (if present) and the domain and protocol. (e.g. /notifications/user)
        * 3. The current date and time stamp added as a custom HTTP header:
        * a. Name: timestamp
        * b. Value: Number of seconds since January 1, 1970 0:00:00 GMT
        * Example: POST/notifications/user1241699805 (in this example timestamp = 1241699805)
        
        * 4. Hash the entire string using the HMAC-SHA246 (Hash Message Authentication Code with SHA256 cryptographic hash function) algorithm with your Private Access Key.
        * 5. Perform URL encoding of the Base64 encoding of the entire hashed string for accurate transmission in the HTTP header.
        */
    
        //		$this->logger->write_to_log('api.log', $privateKey);
        $requestAuthorization = $headers['access_type'].$endpoint_info['endpoint'].$endpoint_info['action'].$headers['timestamp'];
        //		$this->logger->write_to_log('api.log', $requestAuthorization);
        $requestAuthorization = hash_hmac("sha512", $requestAuthorization, $privateKey);
        //		$this->logger->write_to_log('api.log', $requestAuthorization);
        //		$requestAuthorization = urlencode(base64_encode($requestAuthorization));
        //		$this->logger->write_to_log('api.log', $requestAuthorization);
    
        if($requestAuthorization !== $headers['Hash'])
        {
            return ['http_code'=>'403', 'message'=>'Sent hash did not match what the server calculated, request rejected.'];
        }

        return ['http_code'=>'200'];
    }
	
    function checkVerbs(array $request_array = []) : bool
    {
        //$this->logger->write_to_log("api.log", $request_array);
        if(array_key_exists('allowed_verbs', $request_array) && is_array($request_array['allowed_verbs']))
        {
            if(!in_array($request_array['headers']['access_type'], $request_array['allowed_verbs']))
            {
                return false;
            }
            return true;
        }
        return true;
    }

    function generateKeys() : string
    {
        $return['publicKey'] = hash('sha256', openssl_random_pseudo_bytes(32));
        $return['privateKey'] = hash('sha256', openssl_random_pseudo_bytes(32));
    
        return json_encode($return);
    }
}