<?php

class Setup
{
    public string $base, $Host, $Database, $User, $Password, $AppName;

    function __set($name, $value)
    {
    }

    public function __construct()
    {
        $this->base = dirname($_SERVER['DOCUMENT_ROOT']);

        if (php_sapi_name() === "cli") {
            $this->base = dirname(__FILE__, 3);
            //$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        }

        $app_info           = parse_ini_file($this->base . '/global/config.ini', true);
        $this->Host         = $app_info['app_db_info']['host'];      // Hostname of our MySQL server. Set in config.ini.php
        $this->Database     = $app_info['app_db_info']['db_name'];   // Logical database name on that server. Set in config.ini.php
        $this->User         = $app_info['app_db_info']['un'];        // User and Password for login. Set in config.ini.php
        $this->Password     = $app_info['app_db_info']['pw'];
        $this->AppName      = $app_info['app_info']['AppName'];
    }
}
