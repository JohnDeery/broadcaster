<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class TaskMaster
{
	public object $database, $logger;

	function __construct(object $database, object $logger)
	{
		$this->database = $database;
		$this->logger = $logger;
	}

	public function enableDisableTask()
	{
		$sql = "
			SELECT
				status
			FROM
				task_master
			WHERE
				id = :id
		";
		$options['parameters'] = array(':id'=>$_POST['id']);
		$this->database->query($sql, $options);

		switch($this->database->result[0]['status'])
		{
			case 'ready':
				$new_status = 'disabled';
			break;

			default:
				$new_status = 'ready';
			break;
		}

		$sql = "
			UPDATE
				task_master
			SET
				status = :status
			WHERE
				id = :id
		";
		$options['parameters'] = array(':status'=>$new_status, ':id'=>$_POST['id']);
		$this->database->query($sql, $options);
		return array('new_status'=>$new_status);
	}

	public function getAll()
	{
		$sql = "
			SELECT
				id
				, task
				, task_config
				, status
				, last_run_start
				, next_run_start
			FROM
				task_master
			ORDER BY
				id ASC
		";
		$this->database->query($sql);

		foreach($this->database->result as $row)
		{
			foreach($row as $key=>$value)
			{
				if($key == "id")
					continue;

				switch($key)
				{
					case "last_run_start":
					case "next_run_start":
						$value = date("m/d/Y h:i:s", strtotime($value));
					break;

					default:
					break;
				}
				$return[$row['id']][$key] = $value;
			}
		}
		return $return;
	}

	public function getFileConfigValues()
	{
		$file = $_POST['file'];
		$file_contents = file_get_contents(base.'/task_master/tasks/'.$file);
		$comment_start = (strpos($file_contents, "/*") - 1);
		$comment_end = (strpos($file_contents, "*/") - 2);
		$config = json_decode(trim(str_replace(array('/*', '*/'), '', substr($file_contents, $comment_start, $comment_end))));

		return array('data'=>$config);
	}

	public function getTaskDetails($request_array)
	{
		$sql = "
			SELECT
				*
			FROM
				task_master
			WHERE
				id = :id
		";

		$options['parameters'] = array(':id'=>$request_array['post']['id']);
		$this->database->query($sql, $options);
		if($this->database->num_rows > 0)
			return $this->database->result[0];

		return false;
	}

	public function getTaskFiles()
	{
		$valid_tasks = array();
		foreach(glob(base.'/task_master/tasks/*php') as $file)
		{
			$file = explode('/', $file);
			$valid_tasks[] = $file[count($file) -1];
		}
		return array('data'=>$valid_tasks);
	}

	public function getPendingTasks($request_array = NULL)
    {
		$where =" (next_run_start IS NULL OR next_run_start <= NOW()) AND status = :status";
        $options['parameters'] = array(':status' => 'ready');

		if(is_array($request_array) && array_key_exists('forced_task', $request_array))
		{
			$where ="ID = :id";
			$options['parameters'] = array(':id' => $request_array['forced_task']);
		}

        $sql = "
            SELECT
                *
            FROM
                task_master
            WHERE
				$where
        ";
        //$this->logger->write_to_log('tasks.log', $sql);
        $this->database->query($sql, $options);

        if($this->databasedb->num_rows === 0)
            return true;

        return $this->database->result;
    }

    public function updateTaskStatus($params=array())
    {
        if(!is_array($params) || sizeof($params) == 0)
            return FALSE;

        $now = $this->utilities->getUTCDateTime();
        $now = $now->format("y-m-d H:i:s");

		$extra_set = array();

        if(array_key_exists('next_run_start', $params))
           $extra_set[] = "next_run_start = ".$params['next_run_start'];

		if(array_key_exists('last_run_end', $params))
           $extra_set[] = "last_run_end = '".$now."'";

		if(sizeof($extra_set) > 0)
		{
			$extra_set = implode(', ', $extra_set);
		}
		else
		{
			$extra_set = '';
		}

      	$sql = "
            UPDATE
                task_master
            SET
                last_run_start = :time
				, status = :status
				, $extra_set
            WHERE
                id = :id
        ";
        $options['parameters'] = array(':time'=>$now, ':status'=>$params['status'], ':id'=>$params['ID']);
		//$this->logger->write_to_log('tasks_detailed.log', $sql);
		//$this->logger->write_to_log('tasks_detailed.log', $options);

        $this->database->query($sql, $options);

        return TRUE;
    }
}
