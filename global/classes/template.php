<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Template
{
	public $template = 'default', $setup;

	function __construct($setup)
	{
		$this->setup = $setup;
	}

	function buildCss($file = NULL)
	{
		if($file)
		{
			if(file_exists($this->setup->base.DIRECTORY_SEPARATOR.'public_html'.$file))
			{
				$version = $this->getFileVersion(array('file'=>$this->setup->base.DIRECTORY_SEPARATOR.'public_html'.$file));
				echo '<link rel="stylesheet" type="text/css" href="'.$file.'?v='.$version.'">'.PHP_EOL;
			}
		}
		else
		{
			foreach($this->css as $type=>$files)
			{
				if(is_array($files) === TRUE)
				{
					foreach($files as $file)
					{
						//$version_number = filetime('includes/templates/'.$this->template.'/css/'.$files
						echo '<link rel="stylesheet" type="text/css" href="includes/templates/'.$this->template.'/css/'.$files.'">'.PHP_EOL;
					}
				}
				else
				{
					echo '<link rel="stylesheet" type="text/css" href="includes/templates/'.$this->template.'/css/'.$files.'">'.PHP_EOL;
				}
			}
		}
	}

    function buildHeader()
    {
        $nav = '
            <li class="nav-item active">
                <a class="nav-link" href="?module=login" class="button">Log In</a>
            </li>
        ';

        if(array_key_exists('user_info', $_SESSION) && array_key_exists('id', $_SESSION['user_info']) && $_SESSION['user_info']['id'])
		{
            $nav = '
                <li class="nav-item"><a class="nav-link" href="?module=dashboard">Dashboard</a></li>
    			<li class="nav-item"><a class="nav-link" href="?module=upload">Upload</a></li>
                <li class="nav-item"><a class="nav-link" href="?module=bulk_importer">Bulk Import</a></li>
                '.(array_key_exists('user_info', $_SESSION) && array_key_exists('permissions', $_SESSION['user_info']) && in_array('admin', $_SESSION['user_info']['permissions']) ? '<li class="nav-item"><a href="?module=admin" class="nav-link">Site Admin</a></li>' : '').'
    			<li class="nav-item"><a class="nav-link" href="?module=profile">Profile</a></li>
    			<li class="nav-item"><a class="nav-link" href="?module=logout">Logout</a></li>
			';
		}

        echo '
        <header>
            <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: #000;">
                <a class="navbar-brand" href="#">'.$this->setup->AppName.'</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav ml-auto">
                        '.$nav.'
                    </ul>
                </div>
            </nav>
        </header>
        ';
    }

    function buildFooter()
    {
        echo '
            <footer class="fixed-bottom">
                <nav class="navbar navbar-expand-md navbar-secondary bg-secondary">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item"><a class="nav-link">About</a></li>
                        <li class="nav-item"><a class="nav-link">Terms</a></li>
                        <li class="nav-item"><a class="nav-link">Contact</a></li>
                    </ul>
                </nav>
            </footer>
        ';
    }

    function getFileVersion($options)
    {
        return filemtime($options['file']);
    }

    function getVersionedFile($filePath=null)
    {
        echo $filePath.'?v='.$this->getFileVersion(['file'=>$this->base.DIRECTORY_SEPARATOR.'public_html'.$filePath]);
    }
}
