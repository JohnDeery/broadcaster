<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class User
{
	function __construct()
	{
		if (php_sapi_name() == "cli") {
			$this->base = dirname(dirname(dirname(__FILE__)));
		} else {
			$this->base = dirname($_SERVER['DOCUMENT_ROOT']);
		}
		require_once($this->base.'/global/functions.php');
		$this->db = new Database();
		$this->logger = new Logging();
        $this->utilities = new Utilities();
        $this->api = new API();
	}

    function changePassword($request_info=[])
    {
        $request_info['allowed_verbs'] = array("POST");
        if($this->api->checkVerbs($request_info) === FALSE)
        {
            return array('error'=>array('http_code'=> '403', 'message'=>'Method not allowed. Valid methods are: '.implode(', ', $request_info['allowed_verbs'])));
        }

        // get the old password and then check to see if it matches what the user supplied
        $sql = "
            SELECT
                password
            FROM
                users
            WHERE
                id = :id
    	";
        $options['parameters'] = array(':id'=>$_SESSION['user_info']['id']);

        $this->db->query($sql, $options);
        $old_password = $this->db->result[0]['password'];

        if(password_verify($request_info['post']['old_password'], $old_password) === FALSE)
        {
            return array('error'=>array('http_code'=> '401', 'message'=>'Your old password is incorrect. Please try again.'));
        }

        if($request_info['post']['new_password'] != $request_info['post']['confirm_new_password'])
        {
            return array('error'=>array('http_code'=> '401', 'message'=>'The password you supplied don\'t match. Please try again.'));
        }

        $new_password = json_decode($this->generatePassword(array('password'=>$request_info['post']['new_password'])));

        $sql = "
            UPDATE
                users
            SET
                password = :password
            WHERE
                id = :id
        ";
        $options['parameters'] = array(':password'=>$new_password->password, ':id'=> $_SESSION['user_info']['id']);
        $this->db->query($sql, $options);
        return array('message'=>'Your password has been updated');
    }

    function createUser($request_array=[])
    {
        $password = json_decode($this->generatePassword(array('return_clear'=>true)), true);
        $keys = json_decode($this->api->generateKeys(), true);

		$sql = "
			INSERT INTO
				users
			SET
                first_name = :firstName
                , last_name = :lastName
                , email_address = :emailAddress
                , password = :password
                , public_key = :publicKey
                , private_key = :privateKey
                , enabled = 1
                , created_on = :createdOn
                , last_updated = :lastUpdated
		";
        $now = $this->utilities->getUTCDateTime()->format('Y-m-d H:i:s');

        $options['parameters'] = array(
            ':firstName'        =>  (strlen($request_array['endpoint_info']['first_name']) > 0) ? $request_array['endpoint_info']['first_name'] : NULL,
            ':lastName'         =>  (strlen($request_array['endpoint_info']['last_name']) > 0) ? $request_array['endpoint_info']['last_name'] : NULL,
            ':emailAddress'     =>  (strlen($request_array['endpoint_info']['email_address']) > 0) ? $request_array['endpoint_info']['email_address'] : NULL,
            ':password'         =>  $password['password'],
            ':publicKey'        =>  $keys['publicKey'],
            ':privateKey'       =>  $keys['privateKey'],
            ':createdOn'        =>  $now,
            ':lastUpdated'      =>  $now
        );
        $this->db->query($sql, $options);

        if(isset($request_array['endpoint_info']['mail_user']) && (bool)$request_array['endpoint_info']['mail_user'] === true)
        {
  //          $this->logger->write_to_log('general.log', 'mailing credentials to user');
            $mail['session_array'] = config_site();
            $mail['to'] = $request_array['endpoint_info']['email_address'];
            $mail['from'] = 'Broadcastr Administrator';
            $mail['from_address'] = 'administrator@broadcastr.me'; #TODO - dear god, make this dynamic from the DB
            $mail['subject'] = 'Your user account has been created';
            $mail['body'] = 'Welcome to Broadcastr. Your new user account has been created. Your temporary password is '.$password['clear_password'].' which you should immediately change. Thanks!';
            mailMessage($mail);
            $msg = ' and sent an email to change their password';
        }
        return ['msg'=>'User was created'.(isset($msg) ? $msg : '')];
	}

	function generatePassword($options=array())
	{
		$pass = NULL;

		if(!function_exists('password_hash'))
			require_once($this->base.'/global/password.php');

		if(!key_exists('password', $options))
		{
			// Create a random password
			$salt_array = "abcdefghijklmnopqrstuvwxyz0123456789";
			for ($i=0;$i < rand(10, 20);$i++) {
				$tmp = substr($salt_array, rand() % 33, 1);
				if((rand() % 33) < 10 ) { $tmp = strtoupper($tmp); }
				$pass = $pass . $tmp;
			}
		}
		else
		{
			$pass = $options['password'];
		}

		$clearpass = $pass;

		$hash = password_hash($pass, PASSWORD_BCRYPT, array("cost"=>12));

		$return['password'] = $hash;
		if(array_key_exists('return_clear', $options) && $options['return_clear'] === TRUE)
            $return['clear_password'] = $clearpass;

		return json_encode($return);
	}

	function getPermissions($request_array)
	{
		$sql = "
			SELECT
				permission
			FROM
				`user_permissions`
			WHERE
				user_id = :user_id
		";
		$options['parameters'] = array(':user_id' => $request_array['user_id']);
		$this->db->query($sql, $options);

		if($this->db->num_rows() > 0)
			return $this->db->result;

		return FALSE;
	}

/*    public function getUserPreferences()
	{
		$sql = "
			SELECT
				u.preferences AS 'user_prefs',
				d.option_value AS 'defaults'
			FROM
				users u
				LEFT JOIN settings d
			WHERE
				u.ID = :user_id
				AND d.option_name = 'user_preferences_defaults'
		";
		$options = array('parameters'=>array(":user_id"=>$_SESSION['user_info']['id']));

		$this-db->query($sql, $options);

		if(json_decode($db_user_prefs->result[0]['user_prefs']))
			$user_prefs = json_decode($db_user_prefs->result[0]['user_prefs'], TRUE);

		if(json_decode($db_user_prefs->result[0]['defaults']))
			$defaults = json_decode($db_user_prefs->result[0]['defaults'], TRUE);

		// We don't want to pull everything into the sesson, just the value and the type for the preference. This way, if we ever have to change the
		// text that accompanies, we can do so. If the type changes, we want to make sure we override the user pref with the new type and value so
		// we don't get any errors

		$ignored_keys = array("description", "required", "placeholder");

		// For each of the preferences found in the defaults, find out if the user already has one set. If not, we add it to the master array with
		// the default value. We'll also add that to another array so we can alert the user that a new preference was found
		/*
		foreach($defaults as $key=>$value)
		{
			if(array_key_exists($key, $user_prefs) && $user_prefs[$key] != $defaults[$key])
			{
				$preferences[$key] = $user_prefs[$key];
				foreach($ignored_keys as $ignored_key)
					unset($preferences[$key][$ignored_key]);

				$changed++;
			} elseif(!in_array($key, $user_prefs)) {
				$preferences[$key] = $defaults[$key];
				$new_options[] = $key;

				foreach($ignored_keys as $ignored_key)
					unset($preferences[$key][$ignored_key]);
			}
		}
		*/
		/*
		$_SESSION['user_info']['preferences'] = $preferences;

		if($changed > 0)
		{
			$preferences = json_encode($preferences);
			//save_user_prefs();
			$_SESSION['message'] = 'new_preference_found';
		}
	}
*/
	public function getUsers($options=array())
    {
        $limit = '';
        if(array_key_exists('limit', $options)) { $limit = "LIMIT ".$options['limit']; }

        $sql = "
            SELECT
                first_name
                , last_name
                , email_address
            FROM
                `users`
            WHERE
                id > 0
            $limit
        ";
        $this->db->query($sql);
        return $this->db->result;
    }

	public function login($request_info)
	{
		$request_info['allowed_verbs'] = array("POST");
		if($this->api->checkVerbs($request_info) === FALSE)
			return array('error'=>array('http_code'=> '403', 'message'=>'Method not allowed. Valid methods are: '.implode(', ', $request_info['allowed_verbs'])));

		//if($api->checkAuthorization($request_info['headers'], $request_info['endpoint_info'])

		if(!array_key_exists('email_address', $request_info['post']) || strlen($request_info['post']['email_address']) == 0 || !array_key_exists('password', $request_info['post']) || strlen($request_info['post']['password']) == 0)
		{
            return array('error'=>array('http_code'=> '401', 'message'=>'You must supply an email address and password. Please go back and try again.'));
        }

		$sql = "
			SELECT
				id
				, first_name
				, last_name
				, password
				, public_key
				, private_key
			FROM
				users
			WHERE
				email_address = :email_address
		";
		$options['parameters'] = array(':email_address'=>$request_info['post']['email_address']);
		$this->db->query($sql, $options);

		if($this->db->num_rows() == NULL)
		{
			$this->logger->write_to_log('login.log', 'Login attempt for '.$request_info['post']['email_address'].' from '.$_SERVER['REMOTE_ADDR'].' failed - unknown user name.');
			return array('error'=>array('http_code'=> '401', 'message'=>'The email_address or password you provided was not found in our system. Please go back and try again.'));
		}

		$row = $this->db->result[0];

		if(password_verify($request_info['post']['password'], $row['password']))
		{
			$_SESSION['user_info']['id'] = $row['id'];
			$_SESSION['user_info']['first_name'] = $row['first_name'];
			$_SESSION['user_info']['last_name'] = $row['last_name'];

			$this->logger->write_to_log('login.log', 'User '.$_SESSION['user_info']['id'].'('.$request_info['post']['email_address'].') logged in from '.$_SERVER['REMOTE_ADDR']);
//			$this->getUserPreferences();
			$permissions = $this->getPermissions(array('user_id'=>$row['id']));
			if(is_array($permissions) && count($permissions) > 0)
			{
				foreach($permissions as $permission)
				{
					$_SESSION['user_info']['permissions'][] = $permission['permission'];
				}
			}
			return array('html'=>'logged_in', 'api_keys'=>array('pub'=>$row['public_key'], 'priv'=>$row['private_key']));
		} else {
			$this->logger->write_to_log('login.log', 'Login attempt for '.$request_info['post']['email_address'].' from '.$_SERVER['REMOTE_ADDR'].' failed - bad email/password combo.');
			return array('error'=>array('http_code'=>'401', 'message'=>'The email_address or password you provided was not found in our system. Please go back and try again.'));
		}
		return false;
	}
}
