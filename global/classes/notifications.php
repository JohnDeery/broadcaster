<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Notifications
{
	function __construct()
	{
		$this->db = new Database();
	}

	function get_notifications($user_id)
	{
		$notifications = array();

		$sql = "
			SELECT
				*
			FROM
				".$_SESSION['app_config']['app_db_info']['db_name'].".notifications
			WHERE
				user_id = :user_id
			ORDER BY
				date_shown ASC
				, date_entered DESC
			LIMIT 5;
		";
		$parameters = array(':user_id'=>$user_id);
		$options = array('fetch'=>'fetch_assoc', 'parameters'=>$parameters);

		$this->db->query($sql, $options);

		if($this->db->num_rows() == 0)
		{
			$html = 'No Notifications';
		}
		else
		{
			foreach($this->db->Result as $row)
				$notifications[] = $row;

			return $notifications;
		}
	}

	function set_notification($notification=NULL, $user_id=NULL)
	{
		$sql = "
			INSERT INTO
				".$_SESSION['app_config']['app_db_info']['db_name'].".notifications
			SET
				notification = :notification
				, user_id = :user_id
				, date_entered = :date_entered
		";

		$parameters = array(':notification'=>$notification, ':user_id'=>$user_id, ':date_entered'=>convert_to_from_utc(date("Y-m-d H:i:s"), "to"));
		$options = array('parameters'=>$parameters);
		$this->db->query($sql, $options);
	}

    function send_email($info_array)
    {
        if(!isset($info_array['to']) || !isset($info_array['subject']) || !isset($info_array['body'])
        {
            return array('error'=>array('http_code'=> '401', 'message'=>'A required field was missing. Please go back and try again'));
        }

        if(!array_key_exists('from_address', $info_array) || (array_key_exists('from_address', $info_array) && $info_array['from_address'] == ''))
        {
            $info_array['from_address'] = (env == 'cli') ? $info_array['session_array']['site_config']['general_settings']['administrator_email']['value'] : $_SESSION['site_config']['general_settings']['administrator_email']['value'];
        }

        if(!array_key_exists('from', $info_array) || (array_key_exists('from', $info_array) && $info_array['from'] == '')) { $info_array['from'] = 'administrator'; }

        if(!array_key_exists('priority', $info_array) || (array_key_exists('priority', $info_array) && intval($info_array['priority']) == 0)) { $info_array['priority'] = '3'; }

        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->Host = (env == 'cli') ? $info_array['session_array']['site_config']['mail_server_settings']['smtp_server_address']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_server_address']['value'];	// SMTP server
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Port = (env == 'cli') ? $info_array['session_array']['site_config']['mail_server_settings']['smtp_port']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_port']['value'];
        $mail->Username = (env == 'cli') ? $info_array['session_array']['site_config']['mail_server_settings']['smtp_user_name']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_user_name']['value'];	// SMTP username
        $mail->Password = (env == 'cli') ? $info_array['session_array']['site_config']['mail_server_settings']['smtp_password']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_password']['value']; // SMTP password
        $mail->SetFrom($info_array['from_address'], $info_array['from']);

        $mail->Priority = intval($info_array['priority']);
        if(isset($params['bcc'])) { $mail->AddBCC($params['bcc']); }

        $mail_to = explode(',', $params['to']);
        foreach($mail_to as $address) { $mail->AddAddress(trim($address)); }

        $mail->Subject = "[".(env == 'cli' ? $info_array['session_array']['site_config']['general_settings']['app_name']['value'] : $_SESSION['site_config']['general_settings']['app_name']['value'])."] - ".$params['subject'];
        $mail->Body = $info_array['body'];
        $mail->MsgHTML($info_array['body']);

        if(isset($info_array['alt_body'])) { $mail->AltBody = $info_array['alt_body']; }

        $mail->isHTML(true);
        $mail->SetLanguage("en");

        if(!$mail->Send())
        {
            switch(env)
            {
                case 'cli':
                    $logger = new Logging();
                    $logger->write_to_log('error.log', $mail->ErrorInfo);
                    echo $mail->ErrorInfo;
                break;

                default:
                    if($mail->ErrorInfo != "SMTP Error: Data not accepted.")
                    {
                        $_SESSION['error'] = "mailer_too_busy";
                        display_message();
                        exit();
                    }
                break;
            }
        }
        else
        {
            $track_email = (env == 'cli') ? $info_array['session_array']['site_config']['advanced_settings']['track_email']['value'] : $_SESSION['site_config']['advanced_settings']['track_email']['value'];
            if($track_email == 1)
            {
                if(!isset($db_mail_conn))
                    $db_mail_conn = new Database();

                $sql = "
                    INSERT INTO email
                        (`from`, `to`, `subject`, `date_registered`)
                    VALUES
                        ('".$info_array['from_address']."', '".$info_array['to']."', '".$mail->Subject."', NOW())";
                //die($sql);
                $db_mail_conn->query($sql);
            }
        }

        $mail->ClearAddresses();
        return;
    }
}
