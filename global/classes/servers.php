<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Servers
{
    public object $database, $logger;
    
    function __construct($database, $logger)
    {
        $this->database = $database;
        $this->logger = $logger;
    }

    function checkServerStatus($request_array)
    {
        $active = false;

        $cmd = 'nc -w 1 -z '.$request_array['ip_address'].' 22';
        exec($cmd, $output);

        if(isset($output[0]) && strlen($output[0]) > 0 && strpos($output[0], 'succeeded') > -1)
        {
            $active = true;
        }
        return $active;
    }

    function getServers()
    {
        // Get a list of servers to check
        $sql = "
            SELECT
                id
                , server_name
                , ip_address
            FROM
                servers
            WHERE
                active = 'y'
        ";
        $this->database->query($sql);

        foreach($this->database->result as $row)
        {
            //$pingresult = exec("/bin/ping -c2 -w2 {$row['ip_address']}", $outcome, $status);
            $status = ($status === 0) ? 'online' : 'offline';
            $return[$row['server_name']] = [
                'id' => $row['id'],
                'ip_address' => $row['ip_address'],
                'status' => $status
            ];
        }
        return $return;
    }

    function getDiskSpace()
    {
        exec('df', $output);
        foreach($output as $drive)
        {
            $drive_info = explode("\t", $drive);
            print_r($drive_info);
        }
        //return $output;
    }

    function registerServer($request_array)
    {

    }
/*
    function startServer($request_array)
    {
        require 'vendor/autoload.php';
        use Aws\Ec2\Ec2Client;

        $ec2Client = new Aws\Ec2\Ec2Client([
            'region' => 'us-west-2',
            'version' => '2016-11-15',
            'profile' => 'default'
        ]);

        $action = 'START';

        $result = $ec2Client->startInstances(['InstanceIds' => $request_array['instance_id']]);
    }
*/
    function updateServerInfo($request_array)
    {
        foreach($request_array['sets'] as $name=>$value)
        {
            if($name === 'id')
            {
                continue;
            }
            $set[] = "`$name` = :$name";
            $options['parameters'][":$name"] = $value;
        }
        $options['parameters'][":id"] = $request_array['id'];

        $sql = "
            UPDATE
                servers
            SET
        ".implode(PHP_EOL, $set)."
            WHERE
                id = :id
        ";
        $this->database->query($sql, $options);
    }
}
