<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

error_reporting(E_ALL);
ini_set('display_errors', '1');

// get our environment
if (php_sapi_name() === "cli")
{
	define('env', 'cli');
	define('base', dirname(dirname(__FILE__)));
	define('public_html', base.'/public_html/');
}
else
{
	define('env', 'http');
	define('base', dirname($_SERVER['DOCUMENT_ROOT']));
	define('public_html', base.'/');
}

spl_autoload_register('my_autoloader');

function my_autoloader($classname)
{
	switch(strtolower($classname))
	{
		case 'phpmailer':
		case 'smtp':
		case 'pop3':
			$filename = base.'/global/classes/phpmailer/'. strtolower($classname) .'.php';
		break;

		default:
			$filename = base.'/global/classes/'. strtolower($classname) .'.php';
		break;
	}
	require_once($filename);
}

//register_shutdown_function( "fatal_handler" );

/*
function fatal_handler() 
{
	$utilities = new Utilities;

	$errfile = "unknown file";
	$errstr  = "shutdown";
	$errno   = E_CORE_ERROR;
	$errline = 0;

	$error = error_get_last();

	if( $error !== NULL) {

		$errno   = $error["type"];
		$errfile = $error["file"];
		$errline = $error["line"];
		$errstr  = $error["message"];

		error_log($errfile.' on line '.$errline.': '.$errstr, 3, base.'/logs/error.log');

		$mail['to'] = 'john.deery@gmail.com';
		$mail['from'] = 'Broadcastr Administrator';
		$mail['from_address'] = 'john.deery@gmail.com';
		$mail['subject'] = 'Fatal Error';
		$mail['body'] = 'Fatal error in '.$errfile.' on line '.$errline.':'.$errstr;
		$mail['session_array'] = config_site();
		$utilities->mailMessage($mail);
	}
}
*/

/*
function config_site()
{
	return [
		'general_settings' => get_settings('general_settings'),
		'mail_server_settings' => get_settings('mail_server_settings'),
		'advanced_settings' => get_settings('advanced_settings')
	];
}
*/
