#Broadcaster

Broadcaster is your own personal youtube. Host your videos, host the encoding
process, control and secure your videos.

### Changelog


### Technology

Broadcaster uses a suite of standard libraries to help power it:

* PHP
* jQuery
* Bootstrap
* FFMPEG (and associated encoding libraries you choose)


## API Description
Video
 - getAllVideos
 - getVideoDetails (id)
 - getEncodingStatus (id)

User
 - login
 - getAllUsers
 - getUserDetails (id)

Notifications
 - getAll (user_id)
 - markAsRead(id)
 - markAllRead(id)
 - notify(user_id)
