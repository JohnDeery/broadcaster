<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

session_start();
require_once(dirname($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR.'global'.DIRECTORY_SEPARATOR.'functions.php');

$setup = new Setup();
$database = new Database($setup);
$logger = new Logger($setup);

$api = new API($database, $logger);

// Gather information about the request	
$headers = getallheaders();

$headers['access_type'] = $_SERVER['REQUEST_METHOD'];
//$logger->write_to_log('api.log', $headers);
$request_array['headers'] = $headers;

// Endpoint Info
parse_str($_SERVER['REDIRECT_QUERY_STRING'], $endpoint_info);
$request_array['endpoint_info'] = $endpoint_info;

// Body Post
parse_str(file_get_contents('php://input'), $body);
if(count($body) > 0) $request_array['HTTP_RAW_POST_DATA'] = $body;
if(count($_POST) > 0) $request_array['post'] = $_POST;

$logger->write_to_log('api.log', $request_array);

// Rate limiting goes here

// Does our endpoint even exist?
if(!file_exists($setup->base.'/global/classes/'.$endpoint_info['action'].'.php'))
{
	header('HTTP/1.1 404 Endpoint does not exist');
	return;
}

// Start figuring out what we're hitting and if we can do it.
switch($request_array['endpoint_info']['action'])
{
	case "login":
		// Don't need to have auth to do this, we're getting auth from here
		$user = new User();
		$return = $user->login($request_array);
		break;

	default:
		/*
		$isAuthorized = $api->checkAuthorization($headers, $endpoint_info);
		if($isAuthorized === FALSE || (is_array($isAuthorized) && array_key_exists('error', $isAuthorized)))
		{
			$return = $isAuthorized;
		}
		else
		{
			*/
			require_once($_SERVER['DOCUMENT_ROOT'].'modules/'.$endpoint_info['endpoint'].'/api.php');
			$return = $endpoint_info['action']($request_array);
		/*
		}
		*/
	break;
}

$http_code = (isset($return['http_code'])) ? $return['http_code']  : '400';
	
header('HTTP/1.1 '.$http_code.' '.$return['message']);

echo json_encode($return);
return;