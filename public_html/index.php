<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
session_start();
require_once(dirname($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR.'global'.DIRECTORY_SEPARATOR.'functions.php');

if(array_key_exists('module', $_GET) && $_GET['module'] == 'logout')
{
	require_once($_SERVER['DOCUMENT_ROOT'].'modules'.DIRECTORY_SEPARATOR.'logout'.DIRECTORY_SEPARATOR.'index.php');
	return;
}

$setup = new Setup();
$database = new Database($setup);
$logger = new Logger($setup);
$template = new Template($setup);
?>
<!DOCTYPE html>
<html class="bg-light">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content="">

        <title><?php $setup->AppName; ?></title>

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="/includes/templates/default/css/bootstrap.min.css">
        <link rel="stylesheet" href="/includes/templates/default/css/main.css">

        <?php $template->buildCss(DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'font-awesome'.DIRECTORY_SEPARATOR.'5.12.1'.DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.'all.min.css'); ?>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>

		<script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
		<script src="/includes/templates/default/js/bootstrap.bundle.min.js"></script>
		<script src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>
        <script src="/includes/javascript/functions.js?v=1.0"></script>
        <script src="/includes/javascript/jsSHA/2.0.1/src/sha.js?v=1.0"></script>
    </head>

    <body>
        <!--[if lt IE 10]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a></p>
        <![endif]-->
        <?php $template->buildHeader(); ?>
        <div class="container-fluid bg-light p-0" style="margin: 56px auto 60px !important;">
            <div class="section p-3">
<?php
	if(isset($_GET['v']))
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'modules/v/index.php');
	}
	elseif(isset($_GET['module']))
	{
		switch($_GET['module'])
		{
			case 'login':
			case 'register':
            case 'forgot_credentials':
				if(file_exists($_SERVER['DOCUMENT_ROOT'].'modules/'.$_GET['module'].'/index.php'))
				{
					require_once($_SERVER['DOCUMENT_ROOT'].'modules/'.$_GET['module'].'/index.php');
				}
				else
				{
					require_once($_SERVER['DOCUMENT_ROOT'].'modules/home/index.php');
				}
			break;

			default:
				if(array_key_exists('user_info', $_SESSION) && array_key_exists('id', $_SESSION['user_info']) && intval($_SESSION['user_info']['id'] > 0))
				{
					if(file_exists($_SERVER['DOCUMENT_ROOT'].'modules/'.$_GET['module'].'/index.php'))
					{
						require_once($_SERVER['DOCUMENT_ROOT'].'modules/'.$_GET['module'].'/index.php');
					}
					else
					{
						require_once($_SERVER['DOCUMENT_ROOT'].'modules/home/index.php');
					}
				}
				else
				{
					redirect('https://'.$_SERVER['HTTP_HOST']);
				}
			break;
		}
	}
	else
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'modules/home/index.php');
	}
?>
            </div>
        </div>

<?php
    $template->buildFooter();
?>
    </body>
</html>
