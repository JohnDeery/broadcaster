<?php
	session_start();
	
	$_SESSION['site_config']['general_settings']['full_url'] = (stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://').$_SERVER['SERVER_NAME'];

	function __autoload($classname) {
		$filename = $_SERVER['DOCUMENT_ROOT'].'includes/classes/'. strtolower($classname) .'.php';
		require_once($filename);
	}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Broadcastr Setup</title>
<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
<link href='includes/css/main.css' rel='stylesheet' type='text/css'>
</head>

<body>
	<main>
<?	
	if(isset($_POST['Submit']) && $_POST['Submit'] == 'Next')
	{
		switch($_POST['step'])
		{
			case "write_config":
				try
				{
					$dbh = new PDO($_POST['type'].":host=".$_POST['host'],$_POST['db_root_un'],$_POST['db_root_pw']);
				}  
				catch(PDOException $e) {  
					printf("<strong>Database error:</strong> %s<br>%s", 'Something went wrong trying to connect to the database with the CREATE user you submitted. Please review the following message and go back:', $e->getMessage());
					die();
				}

				$_SESSION['app_db_info']['db_root_un'] = $_POST['db_root_un'];
				$_SESSION['app_db_info']['db_root_pw'] = $_POST['db_root_pw'];
								
				if(strstr($_POST['url'], "http"))
				{
					$url = parse_url($_POST['url']);
					$url = $url['host'];
				}
				else
				{
					$url = $_POST['url'];
				}
			
				$config_str = "<?
; This file contains all of the necessary configuration information for the Pollstr system
; Be aware that any changes will affect how the application performs. It's best if you use the gui interface to
; make said changes.

[app_db_info]
host = \"{$_POST['host']}\";
un = \"{$_POST['un']}\";
pw = \"{$_POST['pw']}\";
db_name = \"{$_POST['db_name']}\";
?>";
				if(file_put_contents($_SERVER['DOCUMENT_ROOT'].'../config.ini.php', $config_str) === FALSE)
				{
					echo 'Something went wrong trying to write to the config file. Please make sure you have write permissions to the web directory and try again';
					die();
				}
?>
				<form name="create_tables" method="post">
					<fieldset>
						<legend>Config file created</legend>
						<div>Please click next in order to set up the database.</div>
					</fieldset>
					<input type="hidden" name="step" value="create_tables">
					<input type="submit" name="Submit" value="Next">
					<div class="clearfix"></div>
				</form>
<?php
			break;
			
			case "create_tables":
				require_once($_SERVER['DOCUMENT_ROOT'].'includes/functions.php');

				$user = new User;

				$root_user_name = $_SESSION['app_db_info']['db_root_un'];
				$root_password = $_SESSION['app_db_info']['db_root_pw'];
				
				unset($_SESSION['app_config']);
				config_site(array("app_info"));				
				
				$user_name = $_SESSION['app_db_info']['un'];
				$password = $_SESSION['app_db_info']['pw'];
				
				$_SESSION['app_db_info']['un'] = $root_user_name;
				$_SESSION['app_db_info']['pw'] = $root_password;
				$_SESSION['app_db_info']['no_db'] = 1;

				if(!isset($db))
					$db = new Database();
?>
					<form name="create_tables" method="post">
						<fieldset>
							<legend>Database Creation Results</legend>
<?php
						$sql_files = scandir('sql/');
						
						foreach($sql_files as $key=>$value)
						{
							if($value == "." || $value == "..") continue;
						
							require_once('sql/'.$value);
						}
?>
							<div>Please click next to generate your admin password and finish the installation.</div>
						</fieldset>
						<input type="hidden" name="step" value="generate_admin">
						<input type="submit" name="Submit" value="Next">
						<div class="clearfix"></div>
					</form>
<?php
				break;
				
				case 'generate_admin':
					require_once($_SERVER['DOCUMENT_ROOT'].'includes/functions.php');
					
					if(!isset($db))
						$db = new Database();
				
					$options = array("return_clear"=>TRUE);
					$admin_password = $user->generate_password($options);
					
					$sql = "
						INSERT INTO 
							`users` 
						SET
							user_name = :user_name
							, password = :password
							, enabled = 1
							, created_on = NOW()
					";
	
					$parameters = array(":user_name"=>"administrator", ":password"=>$admin_password['password']);
					$options = array("parameters"=>$parameters);
					$db->query($sql, $options);
					
?>
					<fieldset>
						<legend>Administrator Details</legend>
						<div>Your administrator credentials are as follows: </div>
						<div>
							<label>User Name:</label>Administrator
						</div>
						<div>
							<label>Password:</label><?php echo $admin_password['clear_password']; ?>
						</div>
						<div>Once you log in the first time you will be able to change this.</div>
						<div>
							Please make sure you delete the setup directory to protect your installation from harm
						</div>
						<div>
							Please click on "Next" to finish the setup.
						</div>
					</fieldset>
					<a href="<?php echo $_SESSION['site_config']['general_settings']['full_url']?>" class="button">Finish Installation</a>
					<div class="clearfix"></div>
<?php
					unset($_SESSION);
					
				break;
			}
	}
	else
	{				
?>
		<h1>Welcome to the Setup</h1>
		It looks like this is either your first time here or something happened to your config file. Either way, this setup will help get you started with Pollster. Please follow the prompts in order to get your site all set up.
		<br>
		<br>
		<form name="setup" method="post">
			<fieldset>
				<legend>One-Time Database Information</legend>
					<div>
						In order to set up the database tables, we need a user with CREATE credentials. You may or may not want to this to be the user that you will use for your regular updates. Regardless, we will not store this user information; it will be used only this one time. Afterwards, your database will only be accessed with the credentials in the next section.
					</div>
					<div>
						<label for="db_root_un">User Name</label> 
						<input type="text" name="db_root_un" id="db_root_un" required>
					</div>
					<div>
						<label for="db_root_pw">Password</label> 
						<input type="password" name="db_root_pw" id="db_root_pw" required>
					</div>
			</fieldset>

			<fieldset>
				<legend>Database Information</legend>
					<div>
						This information is about the database for this site. If the database and/or user does not exist, they will be created with the credentials from the above fields.
					</div>
					<div>
						<label for="host">Host</label> 
						<input type="text" name="host" id="host" value="localhost" required>
					</div>
					<div>
						<label for="un">Database User Name</label> 
						<input type="text" name="un" id="un" required>	
						<aside>(if this user does not exist, it will be created)</aside>
					</div>
					<div>
						<label for="pw">Database Password</label> 
						<input type="password" name="pw" id="pw" required>
					</div>
					<div>
						<label for="db_name">Database Name</label> 
						<input type="text" name="db_name" id="db_name" required>
						<aside>(if this database does not exist, it will be created)</aside>
					</div>
					<div>
						<label for="db_type">Database Type</label> 
						<select name="type" id="db_type" required>
							<option value=""> </option>
							<option value="mysql">MySQL</option>
						</select>
					</div>
			</fieldset>
					
			<fieldset>
				<legend>Application Information</legend>
					<div>
						<label for="protocol">Protocol</label>
						<select name="protocol" id="protocol" required>
							<option value=""> </option>
							<option value="http">HTTP</option>
							<option value="https">HTTPS (recommended)</option>
						</select>
					</div>
					<div>
						<label for="url">URL</label> 
						<input type="text" name="url" id="url" placeholder="example.com" required>
						<aside>Please do not include the protocol (http:// or https://) here, the software handles that part</aside>
					</div>
			</fieldset>
			<input type="hidden" name="step" value="write_config">
			<input type="submit" name="Submit" id="Submit" value="Next">
			<div class="clearfix"></div>
		</form>
<?php
}
?>
	</main>
</body>
</html>