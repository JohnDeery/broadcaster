<?php
	$table_name = 'logs';

	$sql = "
		CREATE TABLE IF NOT EXISTS `".$_SESSION['app_config']['app_db_info']['db_name']."`.`$table_name` (
		  `ID` int(11) NOT NULL AUTO_INCREMENT,
		  `log_type` int(11) DEFAULT NULL,
		  `message` text,
		  `user_id` int(11) DEFAULT NULL,
		  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
		  PRIMARY KEY (`ID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	";
	$db->query($sql);
	echo "
		<div>Created table $table_name</div>";
?>
