<div class="row mt-3">
    <div class="col mx-auto">
        <div class="card shadow-sm">
            <a href="?module=manage_users">
                <div class="card-body text-center">
                    <i class="fa fa-users fa-2x" aria-hidden="true"></i><br>Manage Users
                </div>
            </a>
        </div>
    </div>
    <div class="col mx-auto">
        <div class="card">
            <a href="?module=manage_settings">
                <div class="card-body text-center">
                    <i class="fa fa-cogs fa-2x" aria-hidden="true"></i><br>Manage Settings
                </div>
            </a>
        </div>
    </div>
    <div class="col-5 mx-auto">
        <div class="card">
            <h6 class="card-header">Servers <div class="float-right"><a href="?module=manage_servers" class="btn btn-sm btn-primary">Manage</a></div></h6>
            <div class="card-body">
                <table class="table table-sm table-striped table-hover small">
                    <thead>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-5 mx-auto">
        <div class="card">
            <h6 class="card-header">Currently Encoding</h6>
            <div class="card-body">
                <table class="table table-sm table-striped table-hover small">
                    <thead>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                        </tr>
                    </tbody>
            </div>
        </div>
    </div>
</div>
