<?php
	session_start();
	
	require_once($_SERVER['DOCUMENT_ROOT'].'includes/functions.php');

	if(!$db)
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'includes/classes/database.php');
		$db = new Database();
	}

	if(isset($_POST['Submit']))
	{
		switch($_POST['Submit'])
		{
		}
		exit();
	}
	
	switch($_GET['type'])
	{
		default:
?>
	<div class="row">
		<div class="col-lg-12">
<?php
		$sql = "
			SELECT 
				option_value
			FROM
				".$_SESSION['app_config']['app_db_info']['db_name'].".settings
			WHERE
				option_name = :option_name
		";

		$params=array(':option_name'=>'user_preferences_defaults');
		$db->query($sql, array('fetch'=>'single', 'parameters'=>$params));

		if($db->num_rows() > 0)
		{
			$row = $db->Result;
			$preferences = json_decode($row['option_value'], TRUE);
				
			foreach($preferences as $key=>$value)
			{
				echo '
					<div class="setting_wrap">
				';
				
				switch($value['type'])
				{
					case 'timezones':
						echo '
								<label class="nice_looking half">'.ucwords(str_replace('_', ' ', $key)).'</label>
								<select name="timezones">
						';
						$guessed_timezone = date_default_timezone_get();
						$tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
						$selected = ($_SESSION['user_info']['preferences'][$key]['value'] == '') ? $guessed_timezone : $_SESSION['user_info']['preferences'][$key]['value'];

						foreach($tzlist as $tkey=>$tvalue)
						{
							echo '<option value="'.$tvalue.'"'.($tvalue == $selected ? ' selected' : '').'>'.$tvalue.'</option>';
						}
						echo '
								</select>
						';
					
					break;
				
					default:
						echo '
							<label class="nice_looking half">'.ucwords(str_replace('_', ' ', $key)).'</label>
							<input type="text" name="'.$key.'" value="'.$_SESSION['user_info']['preferences'][$key]['value'].'" '.($value['required'] == 1 ? 'required class="required"' : '').'>
						';
					break;
				}
				echo '
							<span class="caption">'.$value['description'].'</span>
						</div>
				';
			}
		} 		
?>
		</div>
	</div>	
	<script type="text/javascript">
		header_inject('script', null, 'text/javascript', '//<?php echo $_SESSION['app_config']['app_info']['url']; ?>/modules/manage_labels/javascript/manage_labels.js');
	</script>
<?php	
		break;
	}
?>