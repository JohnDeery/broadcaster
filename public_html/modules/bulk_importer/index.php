<style>
    table tbody tr:hover { cursor: pointer; }
    table tbody tr.active { color: white; background-color: #8c8c98 !important; }
</style>
<div class="row">
    <div class="col-10 text-right">
        <button class="btn btn-sm btn-outline-secondary" data-action="reload">Reload</button>
    </div>
    <div class="col-2 text-right">
        <button class="btn btn-sm btn-outline-secondary" data-action="selectAll">Select All</button>
    </div>
</div>
<table id="videos" class="table table-sm table-striped table-hover mt-3">
    <thead>
        <tr>
            <th>Video</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
        <tr>
            <td>
                <button data-action="processQueue" class="btn btn-sm btn-outline-secondary float-right" disabled>Process files</button>
            </td>
        </tr>
    </tfoot>
</table>
<script src="/modules/bulk_importer/javascript/bulk_importer.js?v=1.0"></script>
