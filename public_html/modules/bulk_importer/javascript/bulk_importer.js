$(function() {
    getPendingImports();
});

$('#videos tbody').on('click', 'tr.clickable', function(){
    event.stopPropagation();
    if(event.target.type !== 'checkbox')
    {
        $(this).toggleClass('active').find('input[type="checkbox"]').click();
    }

    changeProcessingButton();
});

$('button[data-action="selectAll"]').click(function(){
    $.each($('#videos tbody tr'), function(i){
        $(this).click();
    });
});

$('button[data-action="reload"]').click(function(){
    getPendingImports();
});

$('button[data-action="processQueue"]').click(function(){
    let videos = [];
    $.each($('input:checkbox:checked'), function(i, item){
        videos.push($(item).val());
    });

    createAjaxRequest('POST', 'video/processVideos', {files:videos, source:'bulk_importer'}).done(function(data) {
        if(data)
        {
            alert(data.msg);
            $('#videos tbody').fadeOut();
            changeProcessingButton();
        }
    });
});

function changeProcessingButton()
{
    if($("input:checkbox:checked").length > 0)
    {
        $('button[data-action="processQueue"]').prop('disabled', false);
    }
    else
    {
        $('button[data-action="processQueue"]').prop('disabled', true);
    }
}

function getPendingImports()
{
    $('#videos tbody').html('');
    createAjaxRequest('POST', 'video/getPendingImports').done(function(data) {
        if(data)
        {
            if(data.length === 0)
            {
                $('#videos tbody').append(`<tr><td>Nothing found to import.`);
            }
            else
            {
                $.each(data, function(i, name) {
                    $('#videos tbody').append(`<tr class="clickable"><td><input name="import_videos[]" class="d-none" type="checkbox" value="` + name + `">` + name + `</td></tr>`);
                });
            }
        }
    });
}
