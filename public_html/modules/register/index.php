<link href="/modules/register/register.css" rel="stylesheet" type="text/css">

<div id="register" class="center">
	<form>
		<h2>Please fill out the following information to register a new account</h2>
		<div class="setting_wrap">
			<label for="user_name">User Name</label>
			<input type="text" class="full-width" name="user_name" id="user_name" placeholder="user name" required />
			<div class="clearfix"></div>
		</div>
		<div class="setting_wrap">
			<label for="email">Email Address</label>
			<input type="text" class="full-width" name="email" id="email" placeholder="user@server.com" required />
			<div class="clearfix"></div>
		</div>
		<div class="setting_wrap">
			<label for="password">Password</label>
			<input type="password" class="full-width" name="password" id="password" placeholder="password" required />
			<div class="clearfix"></div>
		</div>
		<div class="setting_wrap">
			<label for="password_confirm">Confirm Password</label>
			<input type="password" class="full-width" name="password_confirm" id="password_confirm" placeholder="password" required />
			<div class="clearfix"></div>
		</div>
	</form>
	<div class="text-center">
		<button data-action="register" class="btn btn-info">Register</button>
	</div>
</div>

<script src="/modules/register/register.js"></script>