$('#changePasswordForm').on('submit', function(e){
    e.preventDefault();

    createAjaxRequest('POST', 'user/changePassword', {'old_password' : $('#old_password').val(), 'new_password' : $('#new_password').val(), 'confirm_new_password' : $('#confirm_new_password').val()}).done(function(data) {
        if(data)
		{
            alert(data.message);
            $('input[type="password"]').val('');
        }
    });
});
