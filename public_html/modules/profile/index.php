<div class="card">
    <form name="changePasswordForm" id="changePasswordForm">
        <div class="card-header">
            Change Password
        </div>
        <div class="card-body">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Old Password</span>
                    </div>
                    <input type="password" class="form-control" name="old_password" id="old_password" required>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">New Password</span>
                    </div>
                    <input type="password" class="form-control" name="new_password" id="new_password" required>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Confirm New Password</span>
                    </div>
                    <input type="password" class="form-control" name="confirm_new_password" id="confirm_new_password" required>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <button class="btn btn-sm btn-outline-primary" data-action="changePassword">Update</button>
        </div>
    </form>
</div>
<script type="text/javascript" src="/modules/profile/javascript/profile.js?v=1.0"></script>
