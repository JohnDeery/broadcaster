<style>
    #videos img {
        width: 100%;
        max-height: 80px;
        object-fit: cover;
        object-position: 0% 0%;
    }

    #videos div.video-controls {
        visibility: hidden;
    }

    #videos tbody tr:hover span.video-description {
        visibility: hidden;
    }

    #videos tbody tr:hover div.video-controls {
        visibility: visible;
    }

</style>
<div id="videos" class="small"></div>

<script type="text/javascript" language="javascript" src="modules/dashboard/javascript/dashboard.js"></script>
