$(function() {
	get_videos();
});

function get_videos()
{
    let html, download_button;
	createAjaxRequest('GET', 'video/getAllVideos').done(function(data) {
		if(Object.entries(data).length === 0)
		{
            $('#videos-thumbnails, #videos-list').html('You don\'t have any videos, start uploading today!');
            return;
        }

        html = `<table class="table table-sm table-striped border">
            <thead>
                <tr>
                    <th>Video</th>
                    <th>Status</th>
                    <th class="text-right">Date Uploaded</th>
                </tr>
            </thead>
            <tbody>
        `;

		$.each(data, function(i, value) {
            var delete_action, delete_text, thumnail_html, download_button;

            switch(value.status)
            {
                case 'pending_delete':
                    delete_action = 'undelete';
                    delete_text = '<i class="fas fa-2x fa-trash-alt" title="Undelete"></i>';
                break;

                case 'deleted':
                    delete_action = null;
                    delete_text = null;
                break;

                default:
                    delete_action = 'delete';
                    delete_text = '<i class="far fa-2x fa-trash-alt" title="Delete"></i>';
                break;
            }

            if(value.status !== 'deleted')
            {
                delete_button = `<div class="col-1"><a class="text-secondary" href="#" data-action="` + delete_action + `">` + delete_text + `</a></div>`;
            }

            download_button = (value.download) ? `<div class="col-1"><a class="text-secondary" href="/videos/` + i + `/` + i + `.zip"><i class="fas fa-2x fa-download" title="Download"></i></a></div>` : '';

            html += `
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-2">
                                <img src="` + value.poster + `">
                                <div class="duration text-white bg-dark px-1" style="position:absolute;right: 18px; bottom: 1px;">` + value.duration + `</div>
                            </div>
                            <div class="col-10">

                                <span class="video-description">
                                    ` + value.title + (value.description ? '<br><span class="small">'+value.description + `</span>` : ``) + `
                                </span>
                                <div class="row flex-fill video-controls">
                                    <div class="col-1 d-flex flex-column h-100">
                                        <a class="text-secondary" href="?module=video_details&id=` + i + `"><i class="fas fa-2x fa-pencil-alt" title="Edit"></i></a>
                                    </div>
                                        ` + delete_button + `
                                        ` + download_button + `
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>` + value.status + `</td>
                    <td class="text-right"> ` + value.date_uploaded + `</td>
                </tr>
            `;
		});
        html += '</tbody></table>';
        $('#videos').append(html);
        $('#videos table').DataTable({
            "order": [[ 2, "desc" ]]
        });
	});
}

$(document).on('click', '[data-action=delete]', function(event) {
	event.preventDefault();
	parent = $(this).closest("tr");
	id = $(parent).data("id");

	$("body").append('<div id="confirm_delete" title="Delete Confirmation">Are you sure you wish to delete this video?</div>');
	$("#confirm_delete").dialog({
		modal: true,
		resizable: false,
		dragable: false,
		buttons: {
			"Yes": function() {
				createAjaxRequest('DELETE', 'video/deleteVideo', {'uid' : id}).done(function() {
    				$("#confirm_delete").dialog( "close" );
                    $('tr[data-id="' +  id + '"] div[data-id="status"]').text('Status: Pending Delete');
                    $('tr[data-id="' +  id + '"] button[data-action="delete"]').attr('data-action', 'undelete').text('Undelete');
				});
			},
			Cancel: function() {
				$("#confirm_delete").dialog( "close" );
			}
		},
		close: function() {
			$("#confirm_delete").remove();
		}
	});
});

$(document).on('click', '[data-action=undelete]', function(event) {
	event.preventDefault();
	parent = $(this).closest("tr");
	id = $(parent).data("id");

    createAjaxRequest('DELETE', 'video/undeleteVideo', {'uid' : id}).done(function() {
        $('tr[data-id="' +  id + '"] div[data-id="status"]').text('Status: ready');
        $('tr[data-id="' +  id + '"] button[data-action="undelete"]').attr('data-action', 'delete').text('Delete');
    });
});
