<?php
// Look to see if we have a thumbnail or not
if(file_exists($_SERVER['DOCUMENT_ROOT'].'/videos/'.$_GET['id'].'/thumbnail.jpg'))
{
        $thumbnail = '/videos/'.$_GET['id'].'/thumbnail.jpg';
}
else
{
        $thumbnail = '/images/thumbnail_not_found.png';
}

$video = new Video();
$template = new Template();

$template->buildCss('/modules/video_details/css/video_details.css');
?>

<div id="video_details" class="card w-75 mx-auto">
    <div class="card-body">
        <div id="video_area" class="mb-3">
            <div id="encoding_progress" class="d-none">
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <div id="video_area" class="d-none"></div>
        </div>

        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo" role="tab" aria-controls="basicInfo" aria-selected="true">Basic Info</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="advancedConfiguration-tab" data-toggle="tab" href="#advancedConfiguration" role="tab" aria-controls="advancedConfiguration" aria-selected="false">Advanced Configuration</a>
            </li>
            <li id="download_tab" class="nav-item">
                <a class="nav-link d-none" id="download-tab" data-toggle="tab" href="#download" role="tab" aria-controls="download" aria-selected="false">Download</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="logs-tab" data-toggle="tab" href="#logs" role="tab" aria-controls="logs" aria-selected="false">Logs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="delete-tab" data-toggle="tab" href="#delete" role="tab" aria-controls="delete" aria-selected="false">Delete</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">
                <div class="card border-top-0">
                    <div class="card-body">
                        <label for="video_name">Title</label>
                        <input type="text" id="video_title">
                        <br><br>
                        <label for="video_description">Description</label><textarea name="video_description" id="video_description"></textarea>
                        <br><br>
                        <input type="hidden" name="frames" id="frames">
                        <button  class="btn btn-sm btn-primary" data-action="save_basic_info" data-id="<?php echo $_GET['id']; ?>">Save</button>
                        <hr>
                        <h3>Thumbnail</h3>
                        <button class="btn btn-sm btn-primary" data-action="generate_thumbnail" data-id="<?php echo $_GET['id']; ?>">Generate Thumbnail</button>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="advancedConfiguration" role="tabpanel" aria-labelledby="advancedConfiguration-tab">
                <div class="card border-top-0">
                    <div class="card-body">
                        <h3>Captions</h3>
                        <table class="table table-sm table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Language</th>
                                    <th>Uploaded</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <input id="fileupload" type="file" name="fileupload" /> 
                        <button class="btn btn-sm btn-outline-primary float-right" data-action="upload_caption">Upload new caption file</button>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="download" role="tabpanel" aria-labelledby="download-tab">
                <div class="card border-top-0">
                    <div class="card-body">
                        <a class="btn btn-sm btn-secondary" href="">Download all files (zip)</a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="logs" role="tabpanel" aria-labelledby="logs-tab">
                <div class="card border-top-0">
                    <div class="card-body">
                        <?php
                            // Get all logs files where the name is like our ID
                            $cmd = 'find '.dirname($_SERVER['DOCUMENT_ROOT']).'/logs/encoding_logs/ -name "'.$_GET['id'].'*"';
                            exec($cmd, $output);

                            if(count($output) == 0)
                            {
                                    echo 'No log files found.';
                            }
                            else
                            {
                            ?>
                            <?php
                                foreach($output as $log)
                                {
                                    $path_array = explode("/", $log);
                                    $extension = explode(".", $path_array[(count($path_array)) - 1]);
                            ?>
                                <button class="btn btn-sm btn-primary" data-action="get_log" data-log="<?php echo $path_array[(count($path_array)) -1]; ?>"><?php echo $extension[1];?></button>
                            <?php
                                }
                            }
                            ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="delete" role="tabpanel" aria-labelledby="delete-tab">
                <div class="card border-top-0">
                    <div class="card-body">
                        Are you sure you want to delete this video? This is an unrecoverable action and will take effect immediately.
                        <button class="btn btn-sm btn-danger" data-action="delete_video" data-id="<?php echo $_GET['id']; ?>">Delete Video</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/modules/video_details/javascript/video_details.js?v=1.0"></script>
