﻿$(function() {
	getVideoDetails();
});

$(document).on('click', '[data-action=get_log]', function(event) {
	event.preventDefault();
	let logfile = $(this).data("log");

	$.ajax({
		type: 'POST',
		cache: false,
		data : {"module":"video_details","Submit":"Submit", "action":"get_log","id":logfile},
		dataType : 'json',
		success : function(data) {
			$("body").append('<div id="modal"><pre>' + data.html + '</pre></div>');
			$("#modal").dialog({
				modal: true,
				resizable: false,
				dragable: false,
				width: "75%",
				height: 500,
				close: function() {
					$("#modal").remove();
					$("#modal").dialog("destroy");
				}
			});
		},
		error: function(request, error) {
			alert("There was an error: " + error);
		}
	});
});

$(document).on('click', '[data-action=generate_thumbnail]', function(event){
	event.preventDefault();
	var id = $(this).data('id');

	$.ajax({
		type: 'POST',
		cache: false,
		data : {"module":"video_details", "Submit":"Submit", "action":"generate_thumbnail","id":id},
		dataType : 'json',
		success : function(data) {
			if(data.return_html)
			{
				console.log('/videos/' + id + '/thumbnail.jpg');
				$("#thumbnail").prop('src', '/videos/' + id + '/thumbnail.jpg');
			}
		}
	});
});

$(document).on('click', '[data-action="save_basic_info"]', function(event) {
    event.preventDefault();

    let id = $(this).data('id');

    createAjaxRequest('PUT', 'video/saveBasicInfo', {id:id, title: $('#video_title').val(), description: $('#video_description').val()}).done(function(data) {
        if(data)
		{
            display_message({"message":"Your settings have been saved", "type":"success"});
        }
    });
});

$(document).on('click', '[data-action="upload_caption"]', function() {
    let formData = new FormData();
    let urlVars = getUrlVars();

    formData.append("file", fileupload.files[0]);

    createAjaxRequest('PUT', 'video/uploadCaption', {'id' : urlVars.id }).done(function(data) {
        if(data)
		{
            console.log(data);
        }
    });
});

function getEncodingProgress()
{
	var urlVars = getUrlVars();
	createAjaxRequest('POST', 'video/getEncodingProgress', {'id' : urlVars.id, 'frames' : parseInt($('#frames').val())}).done(function(data) {
		if(data)
		{
			switch(data.status)
			{
				case 'continue':
					$(".progress-bar").css('width', data.progress + '%').html(data.progress + '% done (' + data.file_type +')');
                    $(document).attr('title', 'Broadcastr - ' + data.progress + '% done (' + data.file_type +')');
					setTimeout(getEncodingProgress, 2000);
				break;

				case 'pending':
					$(".progress-bar").css('width', '0%').html('<span class="justify-content-center text-dark d-flex position-absolute w-100">' + data.file_type + ' conversion is pending</span>');
					setTimeout(getEncodingProgress, 2000);
				break;

				case 'done':
					$(".progress-bar").css('width', '100%').html('Finished');
                    $(document).attr('title', 'Broadcastr');
                    setTimeout(function(){
                        $('#encoding_progress').fadeOut().remove();
                    }, 5000);
                break;
			}
		}
	});
}

function getVideoDetails()
{
	var urlVars = getUrlVars();
	createAjaxRequest('POST', 'video/getVideoDetails', {'id' : urlVars.id}).done(function(data) {
		if (data)
		{
			$("#video_title").val(data.title);
			$("#video_description").val(data.description);
			$("#frames").val(data.frames);

			if (data.status == 'processing' || data.status == 'pending')
			{
				$("#encoding_progress").removeClass('d-none');
				getEncodingProgress();
            }
			else
			{
				$('#video_area').html('<video id="video_' + urlVars.id + '" controls data-setup=\'' + JSON.stringify(data.player) + '\'>');
				$.each(data.video_files, function(i, val) {
					$('#video_area video').append(`<source src="` + val + `" type="` + i + `" />`);
				});
                $('#video_area video').append('<track kind="subtitles" src="https://broadcastr.me/videos/Z005obMBWxzA2o/2017PhysicalTestCC.vtt" srclang="en" label="English" default>');
                if(data.zipfile)
                {
                    $('#download-tab').removeClass('d-none');
                    $('#download a').prop('href', data.zipfile);
                }
			}
        }
	});
}
