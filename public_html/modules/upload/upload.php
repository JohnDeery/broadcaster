<?php
session_start();
require_once(dirname($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR.'global'.DIRECTORY_SEPARATOR.'functions.php');

define("DEBUG", TRUE);

$cleanupTargetDir = true; // Remove old files
$maxFileAge = 5 * 3600; // Temp file age in seconds

if(!array_key_exists('site_config', $_SESSION) || !is_array($_SESSION['site_config']))
{
	config_site();
}

$api = new API();
$db = new Database();
$logger = new Logging();
$video = new Video();
$utilities = new Utilities();

if(count($_FILES) === 0)
{
    return false;
}

// Check just in case the original_videos folder doesn't exist
$targetDir = $utilities->createFolder(['targetDir'=>dirname($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR.'original_videos']);

// Check and make sure we have a video directory
$utilities->createFolder(['targetDir'=>dirname($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR.'public_html'.DIRECTORY_SEPARATOR.'videos']);

// Chunking might be enabled
$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

if(isset($chunk))
{
    $uid = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];
    $tmp_file_name = $uid;
}
else {
    $uid = $video->generateUniqueId();
    $file_info = pathinfo($_FILES["file"]["name"]);
    $tmp_file_name = $uid.'.'.$file_info['extension'];
}

// Put the file in a path that is not in the web accessible root.
$filePath = $targetDir.DIRECTORY_SEPARATOR.$tmp_file_name;

// Remove old temp files
if ($cleanupTargetDir) {
    if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
		die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	}
	while (($file = readdir($dir)) !== false) {
		$tmpfilePath = $targetDir.DIRECTORY_SEPARATOR.$file;
		// If temp file is current file proceed to the next
		if ($tmpfilePath == "{$filePath}.part") {
			continue;
		}
		// Remove temp file if it is older than the max age and is not the current file
		if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
			@unlink($tmpfilePath);
		}
	}
	closedir($dir);
}

// Open temp file
if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
	die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
}
if (!empty($_FILES)) {
	if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
		die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
	}
	// Read binary input stream and append it to temp file
	if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
		die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
	}
} else {
	if (!$in = @fopen("php://input", "rb")) {
		die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
	}
}
while ($buff = fread($in, 4096)) {
	fwrite($out, $buff);
}
@fclose($out);
@fclose($in);
// Check if file has been uploaded
if (!$chunks || $chunk == $chunks - 1) {
	// Strip the temp .part suffix off
    rename("{$filePath}.part", $filePath);

    $fileInfo = pathinfo("$filePath");
    $uid = $video->generateUniqueId();
    $newFilePath = $targetDir.$uid.'.'.$fileInfo['extension'];

    rename("$filePath", "$newFilePath");

    $finalFilePath = $utilities->createFolder(['targetDir'=>$api->base.DIRECTORY_SEPARATOR.'public_html'.DIRECTORY_SEPARATOR.'videos'.DIRECTORY_SEPARATOR.$uid]);

    //$utilities->setPermissions(['target'=>$filePath]);

    // Need to get mediainfo command and do an analysis of videos here with command mediainfo "--Inform=General;IsStreamable: %IsStreamable%" Video.ext
    // if IsStreamable: No then we need to use qt-faststart to fix the file before moving on, otherwise the moov atom is at the end of the file and unuseable
    //https://gist.github.com/alienresident/8949291
    $videoDetails = $video->getOriginalVideoDetails(['filePath'=>$newFilePath]);

    // Enter it into the database so we will know what has happened and have a record of it
    $insert_video_array = [
		'uid'				=>	$uid
		, 'original_name'	=>	(isset($fileInfo['filename']) ? $fileInfo['filename'] : 'NULL')
		, 'file_extension'	=>	(isset($fileInfo['extension']) ? $fileInfo['extension'] : 'NULL')
		, 'file_size'		=>	(isset($videoDetails['format']['size']) ? $videoDetails['format']['size'] : 0)
        , 'duration'        =>  (isset($videoDetails['format']['duration']) ? $videoDetails['format']['duration'] : 0)
		, 'total_frames'	=>	(isset($videoDetails['total_frames']) ? $videoDetails['total_frames'] : 'NULL')
		, 'filename'		=>	(isset($fileInfo['filename']) ? $fileInfo['filename'] : 'NULL')
		, 'owner'			=>	(isset($_SESSION['user_info']['id']) ? $_SESSION['user_info']['id'] : 0)
	];
	$initialInsert = $video->setInitialVideoDetails($insert_video_array);

    // Get the proper encoding types and their commands, then copy into the encoding_queue table
    $sql = "
    	SELECT
    		profiles_to_run
    	FROM
    		encoding_config
    	WHERE
    		file_type = :file_type
    ";
    $options['parameters'] = array(':file_type'=>$fileInfo['extension']);
    $db->query($sql, $options);

    if($db->num_rows() === 1)
    {
    	$profiles = json_decode($db->result[0]['profiles_to_run'], TRUE);
    }
    else
    {
    	// get the defaults
    	$sql = "
    		SELECT
    			profiles_to_run
    		FROM
    			encoding_config
    		WHERE
    			file_type = '*'
    	";
    	$db->query($sql, $options);
    	if($db->num_rows() == 0)
    	{
    		echo 'Error: No profiles to run for this file. Define some then try again';
    		return;
    	}
    	$profiles = json_decode($db->result[0]['profiles_to_run'], TRUE);
    	$logger->write_to_log('sql.log', $profiles);
    }

    // Get the profiles and break out the commands
    $sql = "
    	SELECT
    		cmd
    		, extension
    		, media_type
    	FROM
    		encoding_profiles
    	WHERE
    		profile_name IN ('".implode("','", $profiles)."')
    ";
    $db->query($sql);
    if($db->num_rows() == 0)
    {
    	echo 'Error: Could not parse the encoding profiles, aborting.';
    	return;
    }

    foreach($db->result as $profile)
    {
    	$sql = "
    		INSERT INTO
    			encoding_queue
    		SET
    			uid = :uid
    			, cmd = :cmd
    			, extension = :extension
    			, media_type = :media_type
    			, status = :status
    			, date_created = :date_created
    	";
    	$cmd = json_decode($profile['cmd'], TRUE);

    	foreach($cmd as $sub_cmd)
    	{
    		// Add each sub command into the larger command to place into the queue table.
            $regular_log = '"'.dirname($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'encoding_logs'.DIRECTORY_SEPARATOR.$uid.'.'.$profile['extension'].'.log"';
            $progress_log = '"'.dirname($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'encoding_logs'.DIRECTORY_SEPARATOR.$uid.'.'.$profile['extension'].'.progress.log"';
            $finalFileWithExtension = '"'.$finalFilePath.$uid.'.'.$profile['extension'].'"';
    		$master_command[] = "$video->ffmpeg_cmd -y -progress $progress_log -i \"$newFilePath\" $sub_cmd $finalFileWithExtension > $regular_log 2>&1";
    	}

        $now = $utilities->getUTCDateTime()->format('Y-m-d H:i:s');
    	$options = array('parameters'=>array(':uid'=>$uid, ':cmd'=>implode(" && ", $master_command), ':extension'=>$profile['extension'], ':media_type'=>$profile['media_type'], ':status'=>'pending', ':date_created'=>$now));
    	$db->query($sql, $options);
    	unset($master_command);

        // Finally, mail us so we know everything is done
    	$mail['session_array'] = config_site();
    	$mail['to'] = 'broadcastr@professorials.com';
    	$mail['from'] = 'Broadcastr Administrator';
    	$mail['from_address'] = 'broadcastr@professorials.com';
    	$mail['subject'] = 'A new video has been uploaded';
    	$mail['body'] = 'A new video "'.$fileInfo['filename'].'"('.$uid.') has been uploaded for encoding';
    	mailMessage($mail);
    }

    // Return Success JSON-RPC response
    die('{"jsonrpc" : "2.0", "result" : null, "id" : "'.$uid.'"}');
}
