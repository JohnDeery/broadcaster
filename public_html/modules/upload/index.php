<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

set_time_limit(0);
ini_set('max_input_time', 0);

$video = new Video();
?>

<div id="upload_area" class="card w-50 mx-auto">
    <h5 class="card-header">Please choose a file to upload</h5>
    <div class="card-body">
        <div class="pre-upload">
            <button class="btn btn-small btn-primary" id="select_file">Select File</button>
        </div>
        <div id="upload_progress_area" class="d-none">
            <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript" src="/includes/javascript/jquery/plugins/plupload-3.1.2/js/plupload.full.min.js"></script>
<script type="text/javascript">
	var uploader = new plupload.Uploader({
		runtimes : 'html5',
		browse_button : 'select_file',
		multi_selection : false,
        chunk_size: '5mb',
		container: document.getElementById('upload_area'),
		url : '/modules/upload/upload.php',
		filters : {
			max_file_size : '<?= $video->getMaximumFileUploadSize(); ?>'
			/*mime_types: [
				{title : "Image files", extensions : "jpg,gif,png"},
				{title : "Zip files", extensions : "zip"}
			]*/
		},
		init: {
			FilesAdded: function(up, files) {
				$(".pre-upload").fadeOut().addClass('d-none');
				$("#upload_progress_area").fadeIn().removeClass('d-none');
				window.onbeforeunload = function() {
					return confirm("Leaving this page will cancel your upload, are you sure you want to leave?");
				};
				uploader.start();
			},
			UploadProgress: function(up, file) {
				$("div.determinate").css('width', file.percent + "%");
				$(".progress-bar").html(file.percent + "%").prop('aria-valuenow', file.percent).css('width', 'calc(' + file.percent + '% - 5px)');
			},
			FileUploaded: function(up, file, obj) {
				window.onbeforeunload = null;
				$("#upload_progress_area").fadeOut().addClass('hide');
				var response = jQuery.parseJSON(obj.response);
				window.location.href = '?module=video_details&id=' + response.id;
			},
			Error: function(up, err) {
				alert("\nError #" + err.code + ": " + err.message);
			}
		}
	});
	uploader.init();
</script>

<?php
