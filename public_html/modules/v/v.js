$(function() {
	getVideoDetails();
});

function getVideoDetails()
{
    var videoTagHtml = '',  videoPlayerAttrs = '';
	var urlVars = getUrlVars();

    createAjaxRequest2('POST', 'video/getVideoDetails', {'id' : urlVars.v, 'from':'frontend'}).then(function(data) {
		if (data)
		{
            if(data.poster) { videoPlayerAttrs += 'poster="' + data.poster + '"'; }
            if(data.player)
            {
                if(data.player.autoplay) { videoPlayerAttrs += 'autoplay ' }
                if(data.player.controls) { videoPlayerAttrs += 'controls ' }
                if(data.player.loop) { videoPlayerAttrs += 'loop ' }
                if(data.player.preload) { videoPlayerAttrs += 'preload ' }
            }

            if(data.video_files)
            {
                for (const [type, path] of Object.entries(data.video_files))
                {
                    videoTagHtml += '<source src="' + path + '" type="' + type + '">';
                }
                document.querySelector('#videoArea').innerHTML = '<video '+videoPlayerAttrs+'>' + videoTagHtml + '</video>';
            }

            if(data.title) document.querySelector('#videoTitle').innerHTML = data.title;
        }
	});
}
