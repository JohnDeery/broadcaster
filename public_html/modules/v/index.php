<div class="row">
    <div class="col-1 p-0"></div>
    <div class="col-8 p-0">
        <div id="videoArea" class="text-center embed-responsive embed-responsive-16by9" style="background:black;"></div>
        <h1 id="videoTitle" class="pt-3"></h1>
    </div>
    <div class="col-3">
    </div>
</div>

<script src="/modules/v/v.js?v=1.0"></script>
<script src="/includes/javascript/video_player_hotkeys.js?v=1.0"></script>
