<div class="row">
    <div class="col-lg-6 offset-lg-3 col-md-12">
        <div class="card mt-3 mx-auto">
            <div class="card-body">
                <form id="login" name="login">
                    <h2 class="text-center">Log in</h2>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="john.smith@broadcastr.me" name="email_address" id="email_address" required="required">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password" name="password" id="password" required="required">
                    </div>
                    <div class="form-group">
                        <button id="submit" type="submit" class="btn btn-primary btn-block">Log in</button>
                    </div>
                    <div class="clearfix">
                        <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>
                        <a href="?module=forgot_credentials" class="pull-right">Forgot Password?</a>
                    </div>
                </form>
            </div>
            <!--<p class="text-center"><a href="#">Create an Account</a></p>-->
        </div>
    </div>
</div>
<script src="/modules/login/javascript/login.js"></script>
