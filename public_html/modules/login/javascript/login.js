$(function() {
    $("#email_address").focus();
});

$('form').submit(function(e){
    e.preventDefault();

    $('#submit').html('Logging In <i class="fa fa-circle-o-notch fa-spin fa-fw"></i>').prop('disabled', true);

    $('input[required]').each(function() {
        if(!$(this).val())
        {
            errors = 'You must supply a ' + $(this).prop('placeholder') + '.';
        }
    });

	createAjaxRequest('POST', 'user/login', {'email_address' : $('#login #email_address').val(), 'password' : $('#login #password').val() }).done(function(data) {
        if(data)
		{
            localStorage.setItem('public_key', data.api_keys.pub);
            localStorage.setItem('private_key', data.api_keys.priv);
            window.location.href = '/?module=dashboard';
        }
    }).fail(function() {
        $('#submit').text('Log In').prop('disabled', false);
    });
});
