<a href="?module=admin">Back to admin</a>
<div class="card">
    <h5 class="card-header">
        <div class="row">
            <div class="col-10">Users</div>
            <div class="col-2 text-right"><button class="btn btn-sm btn-outline-primary" data-action="createUser">Create User</button>
        </div>
    </h5>
    <div class="card-body">
        <table class="table table-sm table-striped" data-id="users">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email Address</th>
                </tr>
            </thead>
            <tbody>
                <tr data-id="loader"><td colspan="100" class="text-center"><i class="fa fa-gear fa-spin fa-2x"></i></td></tr>
            </tbody>
        </table>
    </div>
</div>
<div id="user_form" class="d-none">
    <form class="card mt-3">
        <div class="card-body">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">First Name</span>
                </div>
                <input type="text" class="form-control" placeholder="John" name="first_name" id="first_name" required>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Last Name</span>
                </div>
                <input type="text" class="form-control" placeholder="Smith" name="last_name" id="last_name" required>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Email Address</span>
                </div>
                <input type="text" class="form-control" placeholder="someone@somewhere" name="email_address" id="email_address" required>
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-primary">Submit</button>
        </div>
    </form>
</div>
<script type="text/javascript" language="javascript" src="/modules/manage_users/javascript/manage_users.js?v=<?= $template->getFileVersion(array('file'=>$template->base.DIRECTORY_SEPARATOR.'public_html'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'manage_users'.DIRECTORY_SEPARATOR.'javascript'.DIRECTORY_SEPARATOR.'manage_users.js'));?>"></script>
