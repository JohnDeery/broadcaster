$(function() {
	get_users();
});

function get_users()
{
    $('table[data-id="users"] tbody tr[data-id="loader"]').show();
    $('table[data-id="users"] tbody tr').not('[data-id="loader"]').remove();
    createAjaxRequest('GET', 'user/getUsers').done(function(data) {
        $('table[data-id="users"] tr[data-id="loader"]').hide();
        $.each(data, function(i, value) {
            $('table[data-id="users"]').append('<tr data-id="' + value.id + '"><td>' + value.first_name + '</td><td>' + value.last_name + '</td><td>' + value.email_address + '</td></tr>');
        });
    });
}

function resetForm()
{
    $('form input').val('');
    $('form button').prop('disabled', false);
}

$(document).on('click', 'button[data-action="createUser"]', function(e)
{
    e.preventDefault();
    $('#user_form').removeClass('d-none');
});

$(document).on('submit', 'form', function(e)
{
    e.preventDefault();
    $('form button').prop('disabled', true);
    createAjaxRequest('POST', 'user/createUser', {'first_name':$('form #first_name').val(), 'last_name':$('form #last_name').val(), 'email_address':$('form #email_address').val(), 'mail_user' : true}).done(function(data) {
        resetForm();
        alert(data.msg);
    });
});
