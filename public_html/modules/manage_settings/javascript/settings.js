$(function() {
	$("#header_breadcrumb").html("Administration/Settings");
	$(".radioset").buttonset();
	$("textarea").autoResize({ extraSpace: 0 });
});				

$("#add_new_pref").click(function(e) {
	e.preventDefault();
	$("#new_preference").dialog({
		modal : true,
		resizeable : false,
		draggable : false,
		width: "50%"
	});
});
