<a href="?module=admin">Back to admin</a>

<?php
/*
	if(isset($_POST['Submit']))
	{
		switch($_POST['Submit'])
		{
			case "Update Settings":
				$sql = "
					SELECT 
						* 
					FROM 
						".$_SESSION['app_config']['app_db_info']['db_name'].".settings
				";

				$options = array("fetch"=>"fetch_assoc");

				$db->query($sql, $options);
			
				$results = $db->Result;
				foreach($results as $row)
				{
					$sql = "
						UPDATE 
							".$_SESSION['app_config']['app_db_info']['db_name'].".settings 
						SET 
							option_value = '";
	
					$settings_array = NULL;
					$settings = json_decode($row['option_value'], TRUE);
					
					foreach($settings as $key=>$value)
					{
						$settings[$key]['value'] = addslashes($_POST[$key]);
					}
					
					$settings = json_encode($settings);
					
					$sql.=$settings.",";
		
					$sql = trim($sql, ",");
					$sql.="' WHERE option_name = '".$row['option_name']."'";
								
					$db->query($sql);
				}
				$_SESSION['message'] = "item_updated";
				header("Location:?".$_SERVER['QUERY_STRING']);
			break;
		}
	
		exit();
	}

    $template->generateJQuery(array('autoresize'));

	$sql = "
		SELECT 
			* 
		FROM 
			".$_SESSION['app_config']['app_db_info']['db_name'].".settings
	";
	
	$db->query($sql, array('fetch'=>'fetch_assoc'));
	
	if($db->num_rows() > 0)
	{
		echo '<form action="" name="settings" method="post">';

		foreach($db->Result as $row)
		{
			echo '
					<h2>'.ucwords(str_replace("_", " ", $row['option_name'])).'</h2>
						<blockquote class="clear">
			';
			
			$settings = json_decode($row['option_value'], TRUE);
			foreach($settings as $key=>$value)
			{
				echo '
					<div class="form-group">
				';
				
				switch($value['type'])
				{
					case "checkbox":
						echo '
							<label class="checkbox-inline">
								<input type="checkbox" name="'.$key.'" id="'.$key.'" value="1" class="form-control" '.($value['value'] == 1 ? 'checked="checked"' : '').'>'.ucwords(str_replace("_", " ", $key)).'
							</label>		
						';
					break;
					
					case "radioset":
						echo '
								<label class="nice_looking">'.ucwords(str_replace("_", " ", $key)).'</label>
								<div class="radioset">
						';
						foreach($value['possible_values'] as $pkey=>$pvalue)
						{
							echo '
									<input type="radio" id="'.$key.'_'.$pkey.'" name="'.$key.'" value="'.$pvalue.'" '.($pvalue == $value['value'] ? 'checked="checked"' : '').' /><label for="'.$key.'_'.$pkey.'">'.$pkey.'</label>';
						}
						echo '
								</div>
						';
					break;

					case "textarea":
						echo '
								<label for="'.$key.'" class="col-sm-2 control-label">'.ucwords(str_replace("_", " ", $key)).'</label>
								<div class="col-sm-10">
									<textarea name="'.$key.'" id="'.$key.'"'.($value['required'] == 1 ? ' required ' : '').' class="form-control">'.htmlentities(nl2br($value['value'])).'</textarea>
								</div>
						';
					break;
				
					case "password":
					default:
						echo '
							<label for="'.$key.'" class="col-sm-2 control-label">'.ucwords(str_replace("_", " ", $key)).'</label>
							<div class="col-sm-10">
								<input type="'.($value['type'] == 'password' ? 'password' : 'text').'" name="'.$key.'" id="'.$key.'" value="'.htmlentities($value['value']).'"'.($value['required'] == 1 ? ' required ' : '').' '.(strlen($value['placeholder']) > 0 ? 'placeholder="'.$value['placeholder'].'"' : '').' class="form-control">
							</div>
						';
					break;
				}
				echo '
							<div class="col-sm-offset-2 col-sm-10">
								<span class="help-block">'.$value['description'].'</span>
							</div>
						</div>
				';
			}
			
			// Allow administrators to add new settings. This doesn't do much good unless they are going to code something to go along with it, 
			// but it allows for an easier interface than having to mess with the JSON arrays
			if($row['option_name'] == 'user_preferences_defaults')
			{
				echo '<a href="#" class="button" id="add_new_pref">Add a new preference</a>';
			}					
			echo '
						</blockquote>
			';	
		}
		echo '			
						<h2>&nbsp;</h2>
						<br>
						<input type="submit" name="Submit" id="Submit" value="Update Settings" class="btn" />
					</form>
					
					<div id="new_preference" class="hidden">
						<label>Preference Name</label><input type="text" name="name" /><br>
						<label>Preference Type</label>
						<select>
							<option value="text">Text Field</option>
							<option value="textarea">Text Area</option>
						</select>
					</div>
					
		';

	} else {
		echo 'Something is seriously wrong... you have no settings defined and this site can\'t work without them. How are you even here?!';
	}
	echo '
		</div>
	';
	*/

?>