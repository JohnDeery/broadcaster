document.onkeyup = function(e) {
    //console.log(e.which);

    player = document.querySelector('video');

    switch(parseInt(e.which))
    {
        case 32: // space
            if(player.paused === true)
            {
                player.play();
            }
            else {
                player.pause();
            }
        break;

        case 39: // right arrow
            player.currentTime+=10;
        break;

        case 37: // left arrow
            player.currentTime-=10;
        break;

        case 67: // c key

        break;

        case 77: // m key
            if(player.volume === 0)
            {
                player.volume = 1;
            }
            else {
                player.volume = 0;
            }
        break;

        default:
        // do nothing
        break;
    }
};
