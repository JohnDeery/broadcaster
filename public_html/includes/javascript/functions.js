function display_message(message_array)
{
	$("body").showMessage({
		"thisMessage":			message_array.message,
		"className":			message_array,type,
		"opacity":				95,
		"displayNavigation":	false,
		"autoClose":			true,
		"delayTime":			6000
	});
}

$(function() {
    //$('.sidenav').sidenav();
    /*
	$("body").on('click', '[data-action=show_login_box]', function(e) {
		e.preventDefault();

		$("#login_box").toggle();
		if($("#login_box").not(":hidden"))
		{
			$("#login_box #username").focus();
		}
	});

	$("body").on('click', '[data-action=show_top_menu]', function(e) {
		e.preventDefault();

		$("header nav").toggle();
	});

	$("body").on('click', '[data-action=login]', function(e) {
		e.preventDefault();

		var form_data = $('#login_box').serialize();
		$.ajax({
			url : '/api/v1/user/login',
			type : 'POST',
			data : form_data,
			async : false,
			dataType: 'json',
			success : function(data) {
				localStorage.setItem('public_key', data.api_keys.pub);
				localStorage.setItem('private_key', data.api_keys.priv);
				window.location.href = '/?module=dashboard';
			},
			error: function (xhr, ajaxOptions, thrownError) {
				createModal();
				$('#modal').html(thrownError);
				$('#modal').dialog({
					modal: true,
					resizeable: false,
					draggable : false,
					title : 'Error',
					close: function(){
						$('#modal').dialog('close').remove();
					}
				});
			}
		});
	});
    */
});

function createAjaxRequest(postType, method, data)
{
	var promise =  $.ajax({
		url : '/api/v1/' + method,
		type : postType,
		asynch: false,
		cache : false,
		data : data,
		dataType : 'json',
		beforeSend: function(request)
		{
            switch(method)
            {
                case 'user/login':
                break;

                default:
                    apiMethod = method.replace('/', '');
        			var url = 'POST' + apiMethod;
        			var apiHash = createApiHash(url);
        			request.setRequestHeader("Authorization", localStorage.getItem('public_key'));
        			request.setRequestHeader("Hash", apiHash.hmac);
        			request.setRequestHeader("timestamp", apiHash.timestamp);
                break;
            }
		}
	})
	.done(function (responseData, status, xhr) {

	})
	.fail(function(xhr, status, err) {
        alert(err);
        //createModal();
        //$('#modal .modal-body').html(err);
        //$('#modal #modalLabel').html('Error');
        //$('#modal .modal-footer button').removeClass('hidden');
        //$('#modal').addClass('modal-warning').modal();
	});
	return promise;
}

function createAjaxRequest2(verb, endpoint, data)
{
    let headerAuthorization='', headerHash='', headerTimeStamp='', headerContentType = 'application/json';

    switch(endpoint)
    {
        case 'user/login':
        break;

        default:
            let apiMethod = endpoint.replace('/', '');
            let url = verb + apiMethod;
            let apiHash = createApiHash(url);
            headerAuthorization = localStorage.getItem('public_key');
            headerHash = apiHash.hmac;
            headerTimeStamp = apiHash.timestamp;
        break;
    }

    let promise = fetch('/api/v1/' + endpoint, {
        method : verb,
        headers : {'Content-Type': headerContentType, 'Authorization' : headerAuthorization, 'Hash' : headerHash, 'timestamp' : headerTimeStamp },
        body: JSON.stringify(data)
    })
    .then(

    ).catch(function(error) {
        alert(error);
    });

    return promise;
}

function createApiHash(url)
{
	var data = [];

	var timestamp = new Date();
	var unixtime = parseInt(timestamp.getTime() / 1000);
	data.timestamp = unixtime;

	var shaObj = new jsSHA('SHA-512', "TEXT");
	shaObj.setHMACKey(localStorage.getItem('private_key'), "TEXT");
	shaObj.update(url + unixtime);
	var hmac = shaObj.getHMAC("HEX");
	data.hmac = hmac;

	return data;
}

function createModal()
{
	$("#cloneModal").clone().prop("id", "modal").appendTo("body");
}

function getUrlVars()
{
	var vars = [], hash;
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	for(var i = 0; i < hashes.length; i++)
	{
		hash = hashes[i].split('=');

		if($.inArray(hash[0], vars)>-1)
		{
			vars[hash[0]]+=","+hash[1];
		}
		else
		{
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
	}

	return vars;
}
