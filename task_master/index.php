<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

session_start();
require_once(dirname($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR.'global'.DIRECTORY_SEPARATOR.'functions.php');

$setup = new Setup();
$database = new Database($setup);
$logger = new Logger($setup);

$task_master = new TaskMaster($database, $logger);

if(is_array($argv) && count($argv) > 1)
{
	$pending_tasks = $task_master->getPendingTasks(array('forced_task'=>$argv[1]));
}
else
{
	$pending_tasks = $task_master->getPendingTasks();

	if(!is_array($pending_tasks) || count($pending_tasks) == 0)
	{
		$logger->write_to_log('tasks.log', 'No tasks to run');
		return;
	}
}
$logger->write_to_log('tasks.log', 'Found '.sizeof($pending_tasks).' tasks to run');

foreach($pending_tasks as $pending_task)
{
	$task_master->updateTaskStatus(array('status'=>'processing', 'ID'=>$pending_task['ID']));

	// Get the config for the job
	if(strlen($pending_task['task_config']) > 0)
	{
		if(($task_config = json_decode($pending_task['task_config'], TRUE)) == FALSE)
		{
			$task_master->updateTaskStatus(array('status'=>'error', 'ID'=>$pending_task['ID']));
			$logger->write_to_log('tasks.log', 'Task '.$pending_task['task'].' failed: Bad json config');
			return;
		}
	}

	if(is_array($task_config) && array_key_exists('arguments', $task_config))
	{
		foreach($task_config['arguments'] as $argument)
		{
			$argv[] = $argument;
		}
	}
	$logger->write_to_log('tasks.log', 'Running tasks/'.$pending_task['task']);
	$logger->write_to_log('tasks_detailed.log', $task_config);

	require_once($base.'/task_master/tasks/'.$pending_task['file']);

	// Did we change the status that we should set this task back to in the included file? Otherwise, set it back to ready
	$task_status = (isset($task_status)) ? $task_status : 'ready';

	$task_master->updateTaskStatus(array('status'=>$task_status, 'ID'=>$pending_task['ID'], 'last_run_end'=>'1', 'next_run_start'=>"DATE_ADD(NOW(), INTERVAL ".$task_config['run_every'].")"));

	$logger->write_to_log('tasks.log', 'Finished running tasks/'.$pending_task['task'].'.php');
}
