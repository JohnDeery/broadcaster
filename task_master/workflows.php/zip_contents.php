<?php
/*
 *  zip_contents: given a folder path, will zip up all the contents for download
*/

// Zip up the logs to save some space
exec("tar -czf $base/logs/encoding_logs/$uid.tar.gz $base/logs/encoding_logs/$uid.*");
exec("rm $base/logs/encoding_logs/$uid.*.log");
$zip_cmd = 'zip -r -j "'.$video_array['full_video_path'].$uid.'.zip" "'.$video_array['full_video_path'].'"';
exec($zip_cmd);
