<?php
    /*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$db = new Database;
$logger = new Logging;
$video = new Video;
$debug = TRUE;

$sql = "
    SELECT
        uid
        , original_ext
    FROM
        videos
    WHERE
        status = 'pending_delete'
        AND delete_timestamp <= NOW()
";
$db->query($sql);

if($db->num_rows() > 0)
{
    foreach($db->result as $row)
    {
        // Delete the files from the server
        $cmd = "rm -rf $base".DIRECTORY_SEPARATOR."public_html".DIRECTORY_SEPARATOR."videos".DIRECTORY_SEPARATOR.$row['uid'];
        exec($cmd);
//        print_r($cmd);
        $cmd = "rm -rf $base".DIRECTORY_SEPARATOR."original_videos".DIRECTORY_SEPARATOR."{$row['uid']}.{$row['original_ext']}";
        exec($cmd);
//        print_r($cmd);
        $sql = "
            UPDATE
                videos
            SET
                status = 'deleted'
            WHERE
                uid = :uid
        ";
        $options['parameters'] = array(':uid'=>$row['uid']);
        $delete_result = $db->query($sql, $options);
        $logger->write_to_log('deleted_videos.log', "Deleted {$row['uid']} from server");
    }
}