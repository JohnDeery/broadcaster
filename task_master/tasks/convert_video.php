﻿<?php
/*
* Takes pending videos and gets them converting
*
* @author     John Deery <john.deery@gmail.com>
* @version    1.0
*/

$db = new Database;
$logger = new Logging;
$video = new Video;
$utilities = new Utilities;
$api = new API;
$debug = TRUE;

// Make sure we have the final destination Directory
$videosFolder = $utilities->createFolder(['targetDir'=>$api->base.DIRECTORY_SEPARATOR.'public_html'.DIRECTORY_SEPARATOR.'videos']);

if(strlen($videosFolder) === 0)
{
    $logger->write_to_log('encoding.log', 'Failed to create or find the videos folder, we cannot continue.');
    return;
}

$nextVideoForConversion = $video->getNextVideoForConversion();

if(!is_array($nextVideoForConversion) || count($nextVideoForConversion) === 0)
{
	//Nothing do to, just return
	if($debug === TRUE) $logger->write_to_log('encoding.log', 'No videos found to encode, finished.');
	return;
}

if($video->getCurrentlyEncoding() >= 1)
{
	// Too many videos encoding, so do nothing
	if($debug === TRUE) $logger->write_to_log('encoding.log', 'Maximum number of videos encoding reached, skipping.');
	return;
}

$uid = $nextVideoForConversion['uid'];
$owner_email = $nextVideoForConversion['email_address'];
$original_name = $nextVideoForConversion['original_name'];
$original_ext = $nextVideoForConversion['original_ext'];
$encoding_id = $nextVideoForConversion['queue_id'];
$extension = $nextVideoForConversion['extension'];
$cmd = $nextVideoForConversion['cmd'];

// Double check and make sure we have a directory for this to live in, too many times we can fail to create that for various reasons (none acceptable, but here we are)
$folder_exists = $utilities->createFolder(['targetDir'=>$videosFolder.DIRECTORY_SEPARATOR.$uid]);

if(strlen($folder_exists) === 0)
{
    $logger->write_to_log('encoding.log', 'Failed to create or find folder '.$videosFolder.DIRECTORY_SEPARATOR.$uid.', we cannot continue.');
    $video->updateVideoStatus(['status'=>'error', 'uid'=>$uid]);
    return;
}

// check and see if we have any other encodings for this particular video happening right now
$logger->write_to_log('encoding.log', 'Starting '.$extension.' encoding on '.$uid);

// Set the status of the row in the video table to "processing"
$video->updateVideoStatus(['status'=>'processing', 'uid'=>$uid]);

// Set the status of the row in the encoding table to "encoding"
$video->updateEncodingStatus(['status'=>'encoding', 'encoding_server'=>$api->server_name, 'id'=>$encoding_id]);

if($debug === TRUE) $logger->write_to_log('encoding.log', $cmd);
$db->close(); // Close the database connection becaue if this is a long encoding we're going to lose the link but the connector will still think it's there. Next time we try to connect it should get a new link

// Launch the encoding
exec($cmd, $output, $encoding_result);
if($debug === TRUE) $logger->write_to_log('encoding.log', $output);
if($debug === TRUE) $logger->write_to_log('encoding.log', $encoding_result);

// Reset the permissions because they can easily get screwed up
$utilities->setPermissions("$base".DIRECTORY_SEPARATOR."logs".DIRECTORY_SEPARATOR."encoding_logs".DIRECTORY_SEPARATOR."$uid.$extension.log");

if(intval($encoding_result) > 0)
{
    $video->updateEncodingStatus(['status'=>'error', 'id'=>$encoding_id]);
    $video->updateVideoStatus(['status'=>'error', 'id'=>$encoding_id]);
    return;
}

// Set new permissions on the video
if($debug === TRUE) $logger->write_to_log('encoding.log', "$videosFolder".DIRECTORY_SEPARATOR."$uid".DIRECTORY_SEPARATOR."$uid.$extension");
$utilities->setPermissions($videosFolder.DIRECTORY_SEPARATOR.$uid.DIRECTORY_SEPARATOR.$uid.$extension);

// Set the video encoding status to done
$video->updateEncodingStatus(['status'=>'finished', 'id'=>$encoding_id]);
$logger->write_to_log("encoding.log", "Finished encoding $uid to $extension");

// Check and see if we have any more items in the queue for this video. If not, email the owner
$sql = "
	SELECT
		id
	FROM
		encoding_queue
	WHERE
		uid = :uid
		AND status != :status
";
$options['parameters'] = [':uid'=>$uid, ':status'=>'finished'];
$db->query($sql, $options);

if($db->num_rows() === 0)
{
	$video_array = [
		'top_dir' => $base,
		'file' => $uid.'.'.$original_ext,
		'full_video_path' => "$base/public_html/videos/$uid/"
	];

	$video->generateThumbnail($video_array);

	// Zip up the logs to save some space
	exec("tar -czf $base/logs/encoding_logs/$uid.tar.gz $base/logs/encoding_logs/$uid.*");
	exec("rm $base/logs/encoding_logs/$uid.*.log");
    $zip_cmd = 'zip -r -j "'.$video_array['full_video_path'].$uid.'.zip" "'.$video_array['full_video_path'].'"';
    exec($zip_cmd);

	// Set the status of the row in the video table to "ready"
    $video->updateVideoStatus(['status'=>'ready', 'uid'=>$uid]);

	// Finally, mail us so we know everything is done
	$mail['session_array'] = config_site();
	$mail['to'] = $owner_email;
	$mail['cc'] = 'administrator@broadcastr.me';
	$mail['from'] = 'Broadcastr Administrator';
	$mail['from_address'] = 'administrator@broadcastr.me';
	$mail['subject'] = 'Video encoding has been finished';
	$mail['body'] = 'Your video "'.$original_name.'" ('.$uid.') has finished encoding and is ready';
	mailMessage($mail);
}
