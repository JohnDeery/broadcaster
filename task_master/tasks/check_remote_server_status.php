<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$api = new API;
$servers = new Servers;

$server_list = $servers->getServers();

foreach($server_list as $server)
{
    if($server['server_name'] === 'main')
    {
        continue;
    }

    $serverStatus = $servers->checkServerStatus(['ip_address'=>$server['ip_address']]);

    $serverStatus = ((bool)$serverStatus === true) ? 'y' : 'n';

    $servers->updateServerInfo(['id'=>$server['id'], 'sets'=>['active'=>$serverStatus]]);

    if($serverStatus === false)
    {
        // force a umount on the remote server's folder
        //$cmd = 'umount -f -l '.$api->base.DIRECTORY_SEPARATOR.'remote'.DIRECTORY_SEPARATOR.$server['server_name'];
    }
}
