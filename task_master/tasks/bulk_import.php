<?php
/*
 *  broadcaster - converting videos for html5 streaming
 *  Copyright (C) 2016  John Deery (john.deery@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$db = new Database;
$api = new API;
$video = new Video;
$utilities = new Utilities;
$logger = new Logging;

// Do some inits first, make sure we have the appropriate directories created
$originalVideosDir = $utilities->createFolder(['targetDir'=>$api->base.DIRECTORY_SEPARATOR.'original_videos']);
$publicVideosDir = $utilities->createFolder(['targetDir'=>$api->base.DIRECTORY_SEPARATOR.'public_html'.DIRECTORY_SEPARATOR.'videos']);
$endcodingLogsDir = $utilities->createFolder(['targetDir'=>$api->base.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'encoding_logs']);

// Scan the upload directory and see if we have any videos
$file_list = array_diff(scandir($api->base.DIRECTORY_SEPARATOR.'upload'), array('..', '.'));

if(!is_array($file_list) || count($file_list) == 0)
{
	$logger->write_to_log('bulk_import.log', 'No files found to import');
	return;
}

foreach($file_list as $file)
{
    unset($file_info);
    // since we're doing this via FTP, make sure it's actually done being transferred.
//    $safe_file_name = str_replace(' ', '_', $file);
//    exec("grep $safe_file_name /var/log/xferlog", $file_info);

//    if(!is_array($file_info) || count($file_info) === 0)
//    {
//        // We're not done uploading yet, skip
//        continue;
//    }

    $uploadFilePath = $api->base.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.$file;
	$uid = $video->generateUniqueId();
	$owner = 1;

    // Get information about the uploaded file
	$file_info = pathinfo($uploadFilePath);

    // Try to get the broadcastr user id from the file owner, which only really applies in the bulk upload scenario. Do this before we move the file and change it's owner
	$file_owner = posix_getpwuid(fileowner($uploadFilePath));
	$file_owner = (isset($file_owner['name']) && strlen($file_owner['name']) > 0) ? $file_owner['name'] : '';

	if(isset($file_owner) && strlen($file_owner) > 0)
	{
		$sql = "
			SELECT
				id
			FROM
				users
			WHERE
				ftp_user = :file_owner
		";
		$options['parameters'] = array(':file_owner'=>$file_owner);
		$db->query($sql, $options);

		if($db->num_rows() === 1)
		{
			$owner = $db->result[0]['id'];
		}
	}

    // Switch this to a sql lookup since files might exist on different servers
    /*
	while(file_exists($api->base.DIRECTORY_SEPARATOR.'videos'.DIRECTORY_SEPARATOR.$uid.'.'.$file_info['extension']))
    {
		$uid = generate_unique_id();
    }
    */

    // Put the file in a path that is not in the web accessible root.
    $filePath = $originalVideosDir.$uid.'.'.$file_info['extension'];
	$move = rename($uploadFilePath, $filePath);

    if($move === false)
    {
        print_r("FAILED TO MOVE FILE FROM UPLOAD FOLDER TO ORIGINAL_VIDEOS FOLDER, FAILING FOR THIS FILE\n");
        continue;
    }

	// Make sure the ownership and permissions are correct
	chmod($filePath, 0775);
    chown($filePath, 'broadcastr');
    chgrp($filePath, 'apache');

	// Make the final path directory
    $finalFilePath = $utilities->createFolder(['targetDir'=>$publicVideosDir.DIRECTORY_SEPARATOR.$uid]);

	// Make sure the ownership and permissions are correct
	chmod($finalFilePath, 0775);
    chown($finalFilePath, 'broadcastr');
    chgrp($finalFilePath, 'apache');

	$video_details = $video->getOriginalVideoDetails(['filePath'=>$filePath]);

	$insert_video_array = array(
		'uid'				=>	$uid
		, 'original_name'	=>	$file
		, 'file_extension'	=>	$file_info['extension']
		, 'file_size'		=>	$video_details['format']['size']
        , 'duration'        =>  $video_details['format']['duration']
		, 'total_frames'	=>	$video_details['total_frames']
		, 'filename'		=>	$file_info['filename']
		, 'owner'			=>	$owner
	);

	$initialInsert = $video->setInitialVideoDetails($insert_video_array);

	// Get the proper encoding types and their commands, then copy into the encoding_queue table
	$sql = "
		SELECT
			profiles_to_run
		FROM
			encoding_config
		WHERE
			file_type = :file_type
	";
//	if(DEBUG === TRUE) $logger->write_to_log('sql.log', $sql);
	$options['parameters'] = array(':file_type'=>$file_info['extension']);
//	if(DEBUG === TRUE) $logger->write_to_log('sql.log', $options);
	$db->query($sql, $options);

	if($db->num_rows() == 1)
	{
		$profiles = json_decode($db->result[0]['profiles_to_run'], TRUE);
	}
	else
	{
		// get the defaults
		$sql = "
			SELECT
				profiles_to_run
			FROM
				encoding_config
			WHERE
				file_type = '*'
		";
//		if(DEBUG === TRUE) $logger->write_to_log('sql.log', $sql);
		$db->query($sql, $options);
		if($db->num_rows() == 0)
		{
			echo 'Error: No profiles to run for this file. Define some then try again';
			return;
		}
		$profiles = json_decode($db->result[0]['profiles_to_run'], TRUE);
		$logger->write_to_log('sql.log', $profiles);
	}

	// Get the profiles and break out the commands
	$sql = "
		SELECT
			cmd
			, extension
			, media_type
		FROM
			encoding_profiles
		WHERE
			profile_name IN ('".implode("','", $profiles)."')
	";
//	if(DEBUG === TRUE) $logger->write_to_log('sql.log', $sql);
	$db->query($sql);
	if($db->num_rows() == 0)
	{
		echo 'Error: Could not parse the encoding profiles, aborting.';
		return;
	}

	foreach($db->result as $profile)
	{
		$sql = "
			INSERT INTO
				encoding_queue
			SET
				uid = :uid
				, cmd = :cmd
				, extension = :extension
				, media_type = :media_type
				, status = :status
				, date_created = :date_created
		";
//		if(DEBUG === TRUE) $logger->write_to_log('sql.log', $sql);
		$cmd = json_decode($profile['cmd'], TRUE);

		foreach($cmd as $sub_cmd)
		{
			// Add each sub command into the larger command to place into the queue table.
			$master_command[] = $video->ffmpeg_cmd.' -y -progress "'.$endcodingLogsDir.DIRECTORY_SEPARATOR.$uid.'.'.$profile['extension'].'.progress.log" -i "'.$filePath.'" '.$sub_cmd.' "'.$finalFilePath.$uid.'.'.$profile['extension'].'" > "'.$endcodingLogsDir.DIRECTORY_SEPARATOR.$uid.'.'.$profile['extension'].'.log" 2>&1';
		}
//		if(DEBUG === TRUE) $logger->write_to_log('sql.log', $master_command);
		$now = $utilities->getUTCDateTime();
		$options = array('parameters'=>array(':uid'=>$uid, ':cmd'=>implode(" && ", $master_command), ':extension'=>$profile['extension'], ':media_type'=>$profile['media_type'], ':status'=>'pending', ':date_created'=>$now->format("Y-m-d h:i:s")));
		$db->query($sql, $options);
		unset($master_command);
	}
	$logger->write_to_log('bulk_import.log', 'Imported '.$file.' as '.$uid);
}
